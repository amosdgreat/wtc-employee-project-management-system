<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.holidays.index')); ?>"><?php echo app('translator')->get('app.menu.holiday'); ?></a></li>
                <li class="active"><?php echo e($pageTitle); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/calendar/dist/fullcalendar.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css')); ?>">

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <?php $__env->startSection('other-section'); ?>
    <div class="row">
        <div class="col-md-12 show" id="new-follow-panel" style="">
            <h4 id="currentMonthName"></h4>
            
            <table class="table table-hover">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th><?php echo app('translator')->get('app.date'); ?></th>
                    <th><?php echo app('translator')->get('modules.holiday.occasion'); ?></th>
                </tr>
                </thead>
                <tbody id="monthDetailData">

                </tbody>
            </table>
        </div>
    </div>
    <?php $__env->stopSection(); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-11 ">
                        <div class="form-group col-md-2">
                            <label class="control-label"><?php echo app('translator')->get('app.select'); ?> <?php echo app('translator')->get('app.year'); ?></label>
                            <select onchange="getYearData()" class="select2 form-control" data-placeholder="<?php echo app('translator')->get('app.menu.projects'); ?> <?php echo app('translator')->get('app.status'); ?>" id="year">
                                <?php $__empty_1 = true; $__currentLoopData = $years; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $yr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                    <option <?php if($yr == $year): ?> selected <?php endif; ?> value="<?php echo e($yr); ?>"><?php echo e($yr); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>


                <div id="calendar"></div>
            </div>
        </div>

    </div>
    <!-- .row -->
    
    <div class="modal fade bs-modal-md in" id="eventDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>

<script>

    var taskEvents = [
        <?php $__currentLoopData = $holidays; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $holiday): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        {
            id: '<?php echo e(ucfirst($holiday->id)); ?>',
            title: function () {
                var reson = '<?php echo e(ucfirst($holiday->occassion)); ?>';
                if(reson){
                    return reson;
                }
                else{
                    return 'Not Define';
                }
            },
            start: '<?php echo e($holiday->date); ?>',
            end:  '<?php echo e($holiday->date); ?>',
            className:function(){
                var occassion = '<?php echo e($holiday->occassion); ?>';
                if(occassion == 'Sunday' || occassion == 'Saturday'){
                    return 'bg-info';
                }else{
                    return 'bg-danger';
                }
            }
        },
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
];
    var getEventDetail = function (id) {
        var url = '<?php echo e(route('admin.holidays.show', ':id')); ?>';
        url = url.replace(':id', id);

        $('#modelHeading').html('Event');
        $.ajaxModal('#eventDetailModal', url);
    }
    var calendarLocale = '<?php echo e($global->locale); ?>';
    var date = new Date();
    var y = date.getFullYear();
    var d = date.getDate();
    var m = date.getMonth();

    var year = "<?php echo e($year); ?>";

     year =  parseInt(year, 10);
    var defaultDate;

    if(y != year){
         defaultDate = new Date(year, m, d);
         console.log(defaultDate, 'hello');
    }
    else{
         defaultDate = new Date(y, m, d);
    }
    setMonthData(defaultDate);


</script>

<script src="<?php echo e(asset('plugins/bower_components/calendar/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/moment/moment.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/calendar/dist/fullcalendar.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/calendar/dist/jquery.fullcalendar.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/calendar/dist/locale-all.js')); ?>"></script>
<script src="<?php echo e(asset('js/holiday-calendar.js')); ?>"></script>

<script>

    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var currentMonth = new Date();
    $('#currentMonthName').html(monthNames[currentMonth.getMonth()]);
    var currentMonthData = '';

    setMonthData(currentMonth);
    $('.fc-button-group .fc-prev-button').click(function(){
        var bs = $('#calendar').fullCalendar('getDate');
        var d = new Date(bs);
        setMonthData(d);
    });


    $('.fc-button-group .fc-next-button').click(function(){
        var bs = $('#calendar').fullCalendar('getDate');
        var d = new Date(bs);
        setMonthData(d);
    });

    function setMonthData(d){

        var month_int = d.getMonth();
        var year_int = d.getFullYear();
        var firstDay = new Date(year_int, month_int, 1);

        firstDay = moment(firstDay).format("YYYY-MM-DD");

        $('#currentMonthName').html(monthNames[d.getMonth()]);
        var year = "<?php echo e($year); ?>";
        var url = "<?php echo e(route('admin.holidays.calendar-month')); ?>?startDate="+firstDay+"&year="+year;

        var token = "<?php echo e(csrf_token()); ?>";

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                $('#monthDetailData').html(response.data);
            }
        });
    }

    function getYearData(){
        var year = $('#year').val();
        var url = "<?php echo e(route('admin.holidays.calendar', ':year')); ?>";
        url = url.replace(':year', year);
        window.location.href = url;
    }
</script>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/holidays/holiday-calendar.blade.php ENDPATH**/ ?>