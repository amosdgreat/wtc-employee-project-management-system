<?php $__env->startComponent('mail::message'); ?>
# New Task

<?php echo app('translator')->get('email.newTask.subject'); ?>

<h5><?php echo app('translator')->get('app.task'); ?> <?php echo app('translator')->get('app.details'); ?></h5>

<?php $__env->startComponent('mail::text', ['text' => $content]); ?>

<?php echo $__env->renderComponent(); ?>


<?php $__env->startComponent('mail::button', ['url' => $url]); ?>
<?php echo app('translator')->get('app.view'); ?> <?php echo app('translator')->get('app.task'); ?>
<?php echo $__env->renderComponent(); ?>

<?php echo app('translator')->get('email.regards'); ?>,<br>
<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
<?php /**PATH C:\wamp64\www\wtc_emp\resources\views/mail/task/created.blade.php ENDPATH**/ ?>