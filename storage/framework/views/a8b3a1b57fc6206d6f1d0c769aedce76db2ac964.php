<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.leads.index')); ?>"><?php echo e($pageTitle); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('modules.lead.createTitle'); ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <?php echo Form::open(['id'=>'createLead','class'=>'ajax-form','method'=>'POST']); ?>

                            <div class="form-body">
                                <h3 class="box-title"><?php echo app('translator')->get('modules.lead.companyDetails'); ?></h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label required"><?php echo app('translator')->get('modules.lead.companyName'); ?></label>
                                            <input type="text" id="company_name" name="company_name" class="form-control" >
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.lead.website'); ?></label>
                                            <input type="text" id="website" name="website" class="form-control" >
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('app.address'); ?></label>
                                            <textarea name="address"  id="address"  rows="3" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <!--/span-->

                                </div>
                                <!--/row-->

                                <h3 class="box-title"><?php echo app('translator')->get('modules.lead.leadDetails'); ?></h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label for=""><?php echo app('translator')->get('modules.tickets.chooseAgents'); ?>  <a href="javascript:;"
                                                                                                    id="addLeadAgent"
                                                                                                    class="btn btn-sm btn-outline btn-success"><i
                                                            class="fa fa-plus"></i> <?php echo app('translator')->get('app.add'); ?> <?php echo app('translator')->get('app.leadAgent'); ?></a></label>
                                            <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('modules.tickets.chooseAgents'); ?>"  id="agent_id" name="agent_id">
                                                <option value=""><?php echo app('translator')->get('modules.tickets.chooseAgents'); ?></option>
                                                <?php $__currentLoopData = $leadAgents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($emp->id); ?>"><?php echo e(ucwords($emp->user->name)); ?> <?php if($emp->user->id == $user->id): ?>
                                                            (YOU) <?php endif; ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <div class="col-md-6 ">
                                            <div class="form-group">
                                                <label for=""><?php echo app('translator')->get('modules.lead.leadSource'); ?>
                                                        <a href="javascript:;" id="addLeadsource" class="btn btn-sm btn-outline btn-success"><i
                                                                class="fa fa-plus"></i> <?php echo app('translator')->get('app.add'); ?> <?php echo app('translator')->get('modules.lead.leadSource'); ?></a>
                                                </label>
                                                <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('modules.lead.leadSource'); ?>"  id="source_id" name="source_id">
                                                    <?php $__currentLoopData = $sources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $source): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($source->id); ?>"><?php echo e(ucwords($source->type)); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label class="required"><?php echo app('translator')->get('modules.lead.clientName'); ?></label>
                                            <input type="text" name="client_name" id="client_name"  class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="required"><?php echo app('translator')->get('modules.lead.clientEmail'); ?></label>
                                            <input type="email" name="client_email" id="client_email"  class="form-control">
                                            <span class="help-block"><?php echo app('translator')->get('modules.lead.emailNote'); ?></span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <!--/span-->

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.lead.mobile'); ?></label>
                                            <input type="tel" name="mobile" id="mobile" class="form-control">
                                        </div>
                                    </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?php echo app('translator')->get('app.next_follow_up'); ?></label>
                                                <select name="next_follow_up" id="next_follow_up" class="form-control">
                                                        <option value="yes"> <?php echo app('translator')->get('app.yes'); ?></option>
                                                        <option value="no"> <?php echo app('translator')->get('app.no'); ?></option>
                                                </select>
                                            </div>
                                        </div>




                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label><?php echo app('translator')->get('app.note'); ?></label>
                                        <div class="form-group">
                                            <textarea name="note" id="note" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="form-actions">
                                <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>
                                <button type="reset" class="btn btn-default"><?php echo app('translator')->get('app.reset'); ?></button>
                            </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
    
    <div class="modal fade bs-modal-md in" id="projectCategoryModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script type="text/javascript">

    $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });
    $(".date-picker").datepicker({
        todayHighlight: true,
        autoclose: true
    });

    $('#createLead').on('click', '#addLeadAgent', function () {
        var url = '<?php echo e(route('admin.lead-agent-settings.create')); ?>';
        $('#modelHeading').html('Manage Lead Agent');
        $.ajaxModal('#projectCategoryModal', url);
    })

    $('#createLead').on('click', '#addLeadsource', function () {
        var url = '<?php echo e(route('admin.lead-source-settings.create')); ?>';
        $('#modelHeading').html('Manage Lead Source');
        $.ajaxModal('#projectCategoryModal', url);
    })

    $('#save-form').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.leads.store')); ?>',
            container: '#createLead',
            type: "POST",
            redirect: true,
            data: $('#createLead').serialize()
        })
    });

</script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/lead/create.blade.php ENDPATH**/ ?>