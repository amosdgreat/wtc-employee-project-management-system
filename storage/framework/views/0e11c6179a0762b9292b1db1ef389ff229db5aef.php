<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.employees.index')); ?>"><?php echo e($pageTitle); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.details'); ?></li>
            </ol>
        </div>
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12 text-right">
            <a href="<?php echo e(route('admin.employees.edit',$employee->id)); ?>"
               class="btn btn-outline btn-success btn-sm"><?php echo app('translator')->get('modules.lead.edit'); ?>
                <i class="fa fa-edit" aria-hidden="true"></i></a>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<style>
    .counter{
        font-size: large;
    }
</style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
        <!-- .row -->
<div class="row">
    <div class="col-md-5 col-xs-12">
        <div class="white-box">

            <div class="user-bg">
                <img src="<?php echo e($employee->image_url); ?>" alt="user" width="100%">
                <div class="overlay-box">
                    <div class="user-content">
                        <a href="javascript:void(0)"><img src="<?php echo e($employee->image_url); ?>" alt="user"   class="thumb-lg img-circle" width="100%"></a>
                        <h4 class="text-white"><?php echo e(ucwords($employee->name)); ?></h4>
                        <h5 class="text-white"><?php echo e($employee->email); ?></h5>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <div class="col-md-7">
        <div class="user-btm-box">
            <div class="row row-in">
                <div class="col-md-6 row-in-br">
                    <div class="col-in row">
                            <h3 class="box-title"><?php echo app('translator')->get('modules.employees.tasksDone'); ?></h3>
                            <div class="col-xs-4"><i class="ti-check-box text-success"></i></div>
                            <div class="col-xs-8 text-right counter"><?php echo e($taskCompleted); ?></div>
                    </div>
                </div>
                <div class="col-md-6 row-in-br  b-r-none">
                    <div class="col-in row">
                            <h3 class="box-title"><?php echo app('translator')->get('modules.employees.hoursLogged'); ?></h3>
                        <div class="col-xs-2"><i class="icon-clock text-info"></i></div>
                        <div class="col-xs-10 text-right counter" style="font-size: 13px"><?php echo e($hoursLogged); ?></div>
                    </div>
                </div>
            </div>
            <div class="row row-in">
                <div class="col-md-6 row-in-br b-t">
                    <div class="col-in row">
                            <h3 class="box-title"><?php echo app('translator')->get('modules.leaves.leavesTaken'); ?></h3>
                            <div class="col-xs-4"><i class="icon-logout text-warning"></i></div>
                            <div class="col-xs-8 text-right counter"><?php echo e($leavesCount); ?></div>
                    </div>
                </div>
                <div class="col-md-6 row-in-br  b-r-none b-t">
                    <div class="col-in row">
                            <h3 class="box-title"><?php echo app('translator')->get('modules.leaves.remainingLeaves'); ?></h3>
                        <div class="col-xs-4"><i class="icon-logout text-danger"></i></div>
                        <div class="col-xs-8 text-right counter"><?php echo e(($allowedLeaves-$leavesCount)); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="white-box">
            <ul class="nav nav-tabs tabs customtab">
                <li class="active tab"><a href="#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs"><?php echo app('translator')->get('modules.employees.activity'); ?></span> </a> </li>
                <li class="tab"><a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs"><?php echo app('translator')->get('modules.employees.profile'); ?></span> </a> </li>
                <li class="tab"><a href="#projects" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="icon-layers"></i></span> <span class="hidden-xs"><?php echo app('translator')->get('app.menu.projects'); ?></span> </a> </li>
                <li class="tab"><a href="#tasks" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-list"></i></span> <span class="hidden-xs"><?php echo app('translator')->get('app.menu.tasks'); ?></span> </a> </li>
                <li class="tab"><a href="#leaves" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-logout"></i></span> <span class="hidden-xs"><?php echo app('translator')->get('app.menu.leaves'); ?></span> </a> </li>
                <li class="tab"><a href="#time-logs" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-clock"></i></span> <span class="hidden-xs"><?php echo app('translator')->get('app.menu.timeLogs'); ?></span> </a> </li>
                <li class="tab"><a href="#docs" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-docs"></i></span> <span class="hidden-xs"><?php echo app('translator')->get('app.menu.documents'); ?></span> </a> </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <div class="steamline">
                        <?php $__empty_1 = true; $__currentLoopData = $activities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$activity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <div class="sl-item">
                            <div class="sl-left">
                                <?php echo ($employee->image) ? '<img src="'.asset_url('avatar/'.$employee->image).'"
                                                            alt="user" class="img-circle">' : '<img src="'.asset('img/default-profile-2.png').'"
                                                            alt="user" class="img-circle">'; ?>

                            </div>
                            <div class="sl-right">
                                <div class="m-l-40"><a href="#" class="text-info"><?php echo e(ucwords($employee->name)); ?></a> <span  class="sl-date"><?php echo e($activity->created_at->diffForHumans()); ?></span>
                                    <p><?php echo ucfirst($activity->activity); ?></p>
                                </div>
                            </div>
                        </div>
                            <?php if(count($activities) > ($key+1)): ?>
                                <hr>
                            <?php endif; ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <div class="text-center">
                                <div class="empty-space" style="height: 200px;">
                                    <div class="empty-space-inner">
                                        <div class="icon" style="font-size:30px"><i
                                                    class="fa fa-tasks"></i>
                                        </div>
                                        <div class="title m-b-15"><?php echo app('translator')->get('messages.noActivityByThisUser'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="tab-pane" id="profile">
                    <div class="row">
                        <div class="col-xs-6 col-md-4  b-r"> <strong><?php echo app('translator')->get('modules.employees.fullName'); ?></strong> <br>
                            <p class="text-muted"><?php echo e(ucwords($employee->name)); ?></p>
                        </div>
                        <div class="col-xs-6 col-md-4  b-r"> <strong><?php echo app('translator')->get('modules.employees.employeeId'); ?></strong> <br>
                            <p class="text-muted"><?php echo e(ucwords($employee->employeeDetail->employee_id)); ?></p>
                        </div>
                        <div class="col-md-4 col-xs-6 "> <strong><?php echo app('translator')->get('app.email'); ?></strong> <br>
                            <p class="text-muted"><?php echo e($employee->email); ?></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 col-xs-6 b-r"> <strong><?php echo app('translator')->get('app.designation'); ?></strong> <br>
                            <p class="text-muted"><?php echo e((!is_null($employee->employeeDetail) && !is_null($employee->employeeDetail->designation)) ? ucwords($employee->employeeDetail->designation->name) : 'NA'); ?></p>
                        </div>
                        <div class="col-md-4 col-xs-6 b-r"> <strong><?php echo app('translator')->get('app.department'); ?></strong> <br>
                            <p class="text-muted"><?php echo e((!is_null($employee->employeeDetail) && !is_null($employee->employeeDetail->department)) ? ucwords($employee->employeeDetail->department->team_name) : 'NA'); ?></p>
                        </div>
                        <div class="col-md-4 col-xs-6"> <strong><?php echo app('translator')->get('modules.employees.joiningDate'); ?></strong> <br>
                            <p class="text-muted"><?php echo e((!is_null($employee->employeeDetail)) ? $employee->employeeDetail->joining_date->format($global->date_format) : 'NA'); ?></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-6 col-md-4 b-r"> <strong><?php echo app('translator')->get('app.mobile'); ?></strong> <br>
                            <p class="text-muted"><?php echo e($employee->mobile ?? 'NA'); ?></p>
                        </div>
                        <div class="col-md-4 col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.employees.gender'); ?></strong> <br>
                            <p class="text-muted"><?php echo e($employee->gender); ?></p>
                        </div>
                        <div class="col-md-4 col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.employees.slackUsername'); ?></strong> <br>
                            <p class="text-muted"><?php echo e((!is_null($employee->employeeDetail)) ? '@'.$employee->employeeDetail->slack_username : 'NA'); ?></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 col-xs-6"> <strong><?php echo app('translator')->get('modules.employees.hourlyRate'); ?></strong> <br>
                            <p class="text-muted"><?php echo e((!is_null($employee->employeeDetail)) ? $employee->employeeDetail->hourly_rate : 'NA'); ?></p>
                        </div>
                        <div class="col-md-4 col-xs-6"> <strong><?php echo app('translator')->get('app.skills'); ?></strong> <br>
                            <?php echo e(implode(', ', $employee->skills())); ?>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12"> <strong><?php echo app('translator')->get('app.address'); ?></strong> <br>
                            <p class="text-muted"><?php echo e((!is_null($employee->employeeDetail)) ? $employee->employeeDetail->address : 'NA'); ?></p>
                        </div>
                    </div>
                    
                        <?php if(isset($fields)): ?>
                        <div class="row">
                            <hr>
                            <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-6">
                                    <strong><?php echo e(ucfirst($field->label)); ?></strong> <br>
                                    <p class="text-muted">
                                        <?php if( $field->type == 'text'): ?>
                                            <?php echo e($employeeDetail->custom_fields_data['field_'.$field->id] ?? '-'); ?>

                                        <?php elseif($field->type == 'password'): ?>
                                            <?php echo e($employeeDetail->custom_fields_data['field_'.$field->id] ?? '-'); ?>

                                        <?php elseif($field->type == 'number'): ?>
                                            <?php echo e($employeeDetail->custom_fields_data['field_'.$field->id] ?? '-'); ?>


                                        <?php elseif($field->type == 'textarea'): ?>
                                            <?php echo e($employeeDetail->custom_fields_data['field_'.$field->id] ?? '-'); ?>


                                        <?php elseif($field->type == 'radio'): ?>
                                            <?php echo e(!is_null($employeeDetail->custom_fields_data['field_'.$field->id]) ? $employeeDetail->custom_fields_data['field_'.$field->id] : '-'); ?>

                                        <?php elseif($field->type == 'select'): ?>
                                            <?php echo e((!is_null($employeeDetail->custom_fields_data['field_'.$field->id]) && $employeeDetail->custom_fields_data['field_'.$field->id] != '') ? $field->values[$employeeDetail->custom_fields_data['field_'.$field->id]] : '-'); ?>

                                        <?php elseif($field->type == 'checkbox'): ?>
                                            <?php echo e(!is_null($employeeDetail->custom_fields_data['field_'.$field->id]) ? $field->values[$employeeDetail->custom_fields_data['field_'.$field->id]] : '-'); ?>

                                        <?php elseif($field->type == 'date'): ?>
                                            <?php echo e(\Carbon\Carbon::parse($employeeDetail->custom_fields_data['field_'.$field->id])->format($global->date_format)); ?>

                                        <?php endif; ?>
                                    </p>

                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php endif; ?>

                    

                </div>
                <div class="tab-pane" id="projects">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo app('translator')->get('app.project'); ?></th>
                                <th><?php echo app('translator')->get('app.deadline'); ?></th>
                                <th><?php echo app('translator')->get('app.completion'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__empty_1 = true; $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <tr>
                                    <td><?php echo e($key+1); ?></td>
                                    <td><a href="<?php echo e(route('admin.projects.show', $project->id)); ?>"><?php echo e(ucwords($project->project_name)); ?></a></td>
                                    <td><?php if(!is_null($project->deadline)): ?><?php echo e($project->deadline->format($global->date_format)); ?> <?php endif; ?></td>
                                    <td>
                                        <?php

                                        if ($project->completion_percent < 50) {
                                        $statusColor = 'danger';
                                        }
                                        elseif ($project->completion_percent >= 50 && $project->completion_percent < 75) {
                                        $statusColor = 'warning';
                                        }
                                        else {
                                        $statusColor = 'success';
                                        }
                                        ?>

                                        <h5><?php echo app('translator')->get('app.completed'); ?><span class="pull-right"><?php echo e($project->completion_percent); ?>%</span></h5><div class="progress">
                                            <div class="progress-bar progress-bar-<?php echo e($statusColor); ?>" aria-valuenow="<?php echo e($project->completion_percent); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo e($project->completion_percent); ?>%" role="progressbar"> <span class="sr-only"><?php echo e($project->completion_percent); ?>% <?php echo app('translator')->get('app.completed'); ?></span> </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <tr>
                                    <td colspan="4" class="text-center">
                                        <div class="empty-space" style="height: 200px;">
                                            <div class="empty-space-inner">
                                                <div class="icon" style="font-size:30px"><i
                                                            class="icon-layers"></i>
                                                </div>
                                                <div class="title m-b-15"><?php echo app('translator')->get('messages.noProjectFound'); ?>
                                                </div>
                                                <div class="subtitle">
                                                    <a href="<?php echo e(route('admin.projects.create')); ?>" type="button" class="btn btn-info"><i
                                                                class="zmdi zmdi-arrow-left"></i>
                                                        <?php echo app('translator')->get('modules.client.assignProject'); ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="tasks">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="checkbox checkbox-info">
                                <input type="checkbox" id="hide-completed-tasks">
                                <label for="hide-completed-tasks"><?php echo app('translator')->get('app.hideCompletedTasks'); ?></label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                               id="tasks-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo app('translator')->get('app.project'); ?></th>
                                <th><?php echo app('translator')->get('app.task'); ?></th>
                                <th><?php echo app('translator')->get('app.dueDate'); ?></th>
                                <th><?php echo app('translator')->get('app.status'); ?></th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
                <div class="tab-pane" id="leaves">
                    <div class="row">

                        <div class="col-md-12">
                            <ul class="basic-list">
                                <?php $__empty_1 = true; $__currentLoopData = $leaveTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $leaveType): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                    <li><?php echo e(ucfirst($leaveType->type_name)); ?>

                                        <span class="pull-right label-<?php echo e($leaveType->color); ?> label"><?php echo e((isset($leaveType->leavesCount[0])) ? $leaveType->leavesCount[0]->count : '0'); ?></span>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                    <li><?php echo app('translator')->get('messages.noRecordFound'); ?></li>
                                <?php endif; ?>
                            </ul>
                        </div>

                    </div>
                    <hr>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table" id="leave-table">
                                <thead>
                                <tr>
                                    <th><?php echo app('translator')->get('modules.leaves.leaveType'); ?></th>
                                    <th><?php echo app('translator')->get('app.date'); ?></th>
                                    <th><?php echo app('translator')->get('modules.leaves.reason'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__empty_1 = true; $__currentLoopData = $leaves; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$leave): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                    <tr>
                                        <td>
                                            <label class="label label-<?php echo e($leave->type->color); ?>"><?php echo e(ucwords($leave->type->type_name)); ?></label>
                                        </td>
                                        <td>
                                            <?php echo e($leave->leave_date->format($global->date_format)); ?>

                                        </td>
                                        <td>
                                            <?php echo e($leave->reason); ?>

                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                    <tr>
                                        <td colspan="3" class="text-center">
                                            <div class="empty-space" style="height: 200px;">
                                                <div class="empty-space-inner">
                                                    <div class="icon" style="font-size:30px"><i
                                                                class="icon-logout"></i>
                                                    </div>
                                                    <div class="title m-b-15"><?php echo app('translator')->get('messages.noRecordFound'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>

                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="time-logs">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="timelog-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo app('translator')->get('app.project'); ?></th>
                                <th><?php echo app('translator')->get('modules.employees.startTime'); ?></th>
                                <th><?php echo app('translator')->get('modules.employees.endTime'); ?></th>
                                <th><?php echo app('translator')->get('modules.employees.totalHours'); ?></th>
                                <th><?php echo app('translator')->get('modules.employees.memo'); ?></th>
                            </tr>
                            </thead>
                        </table>
                    </div>


                </div>
                <div class="tab-pane" id="docs">

                    <button class="btn btn-sm btn-info addDocs" onclick="showAdd()"><i
                                class="fa fa-plus"></i> <?php echo app('translator')->get('app.add'); ?></button>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th width="70%"><?php echo app('translator')->get('app.name'); ?></th>
                                <th><?php echo app('translator')->get('app.action'); ?></th>
                            </tr>
                            </thead>
                            <tbody id="employeeDocsList">
                            <?php $__empty_1 = true; $__currentLoopData = $employeeDocs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$employeeDoc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <tr>
                                    <td><?php echo e($key+1); ?></td>
                                    <td width="70%"><?php echo e(ucwords($employeeDoc->name)); ?></td>
                                    <td>
                                        <a href="<?php echo e(route('admin.employee-docs.download', $employeeDoc->id)); ?>"
                                           data-toggle="tooltip" data-original-title="Download"
                                           class="btn btn-primary btn-circle"><i
                                                    class="fa fa-download"></i></a>
                                        <a target="_blank" href="<?php echo e($employeeDoc->doc_url); ?>"
                                           data-toggle="tooltip" data-original-title="View"
                                           class="btn btn-info btn-circle"><i
                                                    class="fa fa-search"></i></a>
                                        <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" data-file-id="<?php echo e($employeeDoc->id); ?>"
                                                                                    data-pk="list" class="btn btn-danger btn-circle sa-params"><i class="fa fa-times"></i></a>
                                    </td>

                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <tr>
                                <tr>
                                    <td colspan="3" class="text-center">
                                        <div class="empty-space" style="height: 200px;">
                                            <div class="empty-space-inner">
                                                <div class="icon" style="font-size:30px"><i
                                                            class="fa fa-dashcube"></i>
                                                </div>
                                                <div class="title m-b-15"><?php echo app('translator')->get('messages.noDocsFound'); ?>
                                                </div>
                                                <div class="subtitle">
                                                    <button onclick="showAdd()" type="button" class="btn btn-info">
                                                        <i class="fa fa-plus"></i>
                                                        <?php echo app('translator')->get('app.add'); ?>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
        
        <div class="modal fade bs-modal-md in" id="edit-column-form" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-md" id="modal-data-application">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                    </div>
                    <div class="modal-body">
                        Loading...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn blue">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script>
    // Show Create employeeDocs Modal
    function showAdd() {
        var url = "<?php echo e(route('admin.employees.docs-create', [$employee->id])); ?>";
        $.ajaxModal('#edit-column-form', url);
    }
    showTable();

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('file-id');
        var deleteView = $(this).data('pk');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "<?php echo e(route('admin.employee-docs.destroy',':id')); ?>";
                url = url.replace(':id', id);

                var token = "<?php echo e(csrf_token()); ?>";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                    success: function (response) {
                        console.log(response);
                        if (response.status == "success") {
                            $.unblockUI();
                            $('#employeeDocsList').html(response.html);
                        }
                    }
                });
            }
        });
    });

    $('#leave-table').dataTable({
        responsive: true,
        "columnDefs": [
            { responsivePriority: 1, targets: 0, 'width': '20%' },
            { responsivePriority: 2, targets: 1, 'width': '20%' }
        ],
        "autoWidth" : false,
        searching: false,
        paging: false,
        info: false
    });

    var table;

    function showTable() {
        if ($('#hide-completed-tasks').is(':checked')) {
            var hideCompleted = '1';
        } else {
            var hideCompleted = '0';
        }

        var url = '<?php echo e(route('admin.employees.tasks', [$employee->id, ':hideCompleted'])); ?>';
        url = url.replace(':hideCompleted', hideCompleted);

        table = $('#tasks-table').dataTable({
            destroy: true,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: url,
            deferRender: true,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function (oSettings) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            "order": [[0, "desc"]],
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                {data: 'project_name', name: 'projects.project_name', width: '20%'},
                {data: 'heading', name: 'heading', width: '20%'},
                {data: 'due_date', name: 'due_date'},
                {data: 'status', name: 'status'}
            ]
        });
    }

    $('#hide-completed-tasks').click(function () {
        showTable();
    });


</script>

<script>
    var table2;

    function showTable2(){

        var url = '<?php echo e(route('admin.employees.time-logs', [$employee->id])); ?>';

        table2 = $('#timelog-table').dataTable({
            destroy: true,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: url,
            deferRender: true,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            "order": [[ 0, "desc" ]],
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'project_name', name: 'projects.project_name' },
                { data: 'start_time', name: 'start_time' },
                { data: 'end_time', name: 'end_time' },
                { data: 'total_hours', name: 'total_hours' },
                { data: 'memo', name: 'memo' }
            ]
        });
    }

    showTable2();
</script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/employees/show.blade.php ENDPATH**/ ?>