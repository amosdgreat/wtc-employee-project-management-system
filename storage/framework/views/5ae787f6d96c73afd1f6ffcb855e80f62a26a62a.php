<li class="top-notifications">
    <div class="message-center">
        <a href="javascript:;" class="show-all-notifications">
            <div class="user-img">
                <span class="btn btn-circle btn-info"><i class="icon-list"></i></span>
            </div>
            <div class="mail-contnet">
                <span class="mail-desc m-0"><?php echo e(__('email.newTask.subject')); ?> - <?php echo e(ucfirst($notification->data['heading'])); ?></span> <span class="time"><?php if($notification->created_at): ?><?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $notification->created_at)->diffForHumans()); ?><?php endif; ?></span>
            </div>
        </a>
    </div>
</li>
<?php /**PATH C:\wamp64\www\wtc_emp\resources\views/notifications/member/new_task.blade.php ENDPATH**/ ?>