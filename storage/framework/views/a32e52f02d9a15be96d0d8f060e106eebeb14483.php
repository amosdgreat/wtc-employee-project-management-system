<div class="media">
    <div class="media-body">
        <h5 class="media-heading"><span class="btn btn-circle btn-success"><i class="icon-user"></i></span> <?php echo app('translator')->get('app.welcome'); ?> <?php echo app('translator')->get('app.to'); ?> <?php echo e($companyName); ?> !</h5>
    </div>
    <h6><i><?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $notification->created_at)->diffForHumans()); ?></i></h6>
</div>
<?php /**PATH C:\wamp64\www\wtc_emp\resources\views/notifications/member/detail_new_user.blade.php ENDPATH**/ ?>