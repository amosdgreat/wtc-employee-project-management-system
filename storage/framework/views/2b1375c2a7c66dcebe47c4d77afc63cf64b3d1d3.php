<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.currency.index')); ?>"><?php echo e($pageTitle); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.update'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">

                <div class="vtabs customvtab m-t-10">
                    <?php echo $__env->make('sections.notification_settings_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="tab-content">
                        <div id="vhome3" class="tab-pane active">
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <?php echo Form::open(['id'=>'updateCurrency','class'=>'ajax-form','method'=>'PUT','autocomplete'=>"off"]); ?>

                                            <?php echo method_field('PUT'); ?>
                                            <div class="form-group">
                                                <label for="currency_name">PUSHER APP ID</label>
                                                <input type="text" readonly="readonly" onfocus="this.removeAttribute('readonly');" class="form-control auto-complete-off" id="pusher_app_id" name="pusher_app_id" value="<?php echo e($pusherSettings->pusher_app_id); ?>">
                                            </div>

                                            <div class="form-group" >
                                                <label for="currency_symbol">PUSHER APP KEY</label>
                                                <input type="password" class="form-control" id="pusher_app_key" name="pusher_app_key" value="<?php echo e($pusherSettings->pusher_app_key); ?>">
                                                <span class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="currency_code">PUSHER APP SECRET</label>
                                                <input type="password" class="form-control" id="pusher_app_secret" name="pusher_app_secret" value="<?php echo e($pusherSettings->pusher_app_secret); ?>">
                                                <span class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="currency_code">PUSHER CLUSTER</label>
                                                <input type="text" class="form-control" id="pusher_cluster" name="pusher_cluster" value="<?php echo e($pusherSettings->pusher_cluster); ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="currency_code">Force TLS</label>
                                                <select name="force_tls" id="force_tls" class="form-control">
                                                    <option value="0"
                                                    <?php if($pusherSettings->force_tls == "0"): ?>
                                                        selected
                                                    <?php endif; ?>
                                                    >False</option>
                                                    <option value="1"
                                                    <?php if($pusherSettings->force_tls == "1"): ?>
                                                        selected
                                                    <?php endif; ?>

                                                >True</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label" ><?php echo app('translator')->get('app.status'); ?></label>
                                                <div class="switchery-demo">
                                                    <input type="checkbox" name="status" <?php if($pusherSettings->status): ?> checked  <?php endif; ?> class="js-switch " data-color="#00c292" data-secondary-color="#f96262"  />
                                                </div>
                                            </div>


                                            <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                                                <?php echo app('translator')->get('app.save'); ?>
                                            </button>
                                            <button type="reset" class="btn btn-inverse waves-effect waves-light"><?php echo app('translator')->get('app.reset'); ?></button>
                                            <?php echo Form::close(); ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.js')); ?>"></script>
<script>
    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
    });

    $('#save-form').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.pusher-settings.update', $pusherSettings->id )); ?>',
            container: '#updateCurrency',
            type: "POST",
            data: $('#updateCurrency').serialize()
        })
    });
</script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/pusher-settings/edit.blade.php ENDPATH**/ ?>