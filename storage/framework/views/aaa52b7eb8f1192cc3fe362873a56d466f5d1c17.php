<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e($pageTitle); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"><?php echo app('translator')->get('modules.pushSettings.updateTitle'); ?></div>

                <div class="vtabs customvtab m-t-10">

                    <?php echo $__env->make('sections.notification_settings_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="tab-content">
                        <div id="vhome3" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-6">

                                    <h3 class="box-title m-b-0"><?php echo app('translator')->get("modules.slackSettings.notificationTitle"); ?></h3>

                                    <p class="text-muted m-b-10 font-13">
                                        <?php echo app('translator')->get("modules.slackSettings.notificationSubtitle"); ?>
                                    </p>

                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12 b-t p-t-20">
                                            <?php echo Form::open(['id'=>'editSettings','class'=>'ajax-form form-horizontal','method'=>'PUT','autocomplete'=>"off"]); ?>


                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.employeeAssign"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[5]->send_push == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[5]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.taskAssign"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[7]->send_push == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[7]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.expenseAdded"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[0]->send_push == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[0]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.expenseMember"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[1]->send_push == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[1]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.expenseStatus"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[2]->send_push == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[2]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.ticketRequest"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[3]->send_push == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[3]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.leaveRequest"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[8]->send_push == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[8]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.taskComplete"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[9]->send_push == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[9]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.invoiceNotification"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[10]->send_push == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[10]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>


                                            <?php echo Form::close(); ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <?php echo Form::open(['id'=>'editSlackSettings','class'=>'ajax-form','method'=>'PUT']); ?>



                                    <h5>
                                        Signup on <a href="https://onesignal.com/" target="_blank">onesignal.com</a>
                                    </h5>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label for="company_name"><?php echo app('translator')->get('modules.pushSettings.oneSignalAppId'); ?></label>

                                            <input type="text" class="form-control" id="onesignal_app_id"
                                                   name="onesignal_app_id" value="<?php echo e($pushSettings->onesignal_app_id); ?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="company_name"><?php echo app('translator')->get('modules.pushSettings.oneSignalRestApiKey'); ?></label>


                                            <input type="password" readonly="readonly" onfocus="this.removeAttribute('readonly');" class="form-control auto-complete-off" id="onesignal_rest_api_key"
                                                   name="onesignal_rest_api_key" value="<?php echo e($pushSettings->onesignal_rest_api_key); ?>">
                                            <span class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        </div>

                                        <div class="form-group">
                                            <label for="company_name"><?php echo app('translator')->get('app.status'); ?></label>
                                            <select name="status" class="form-control" id="">
                                                <option
                                                        <?php if($pushSettings->status == 'inactive'): ?> selected <?php endif; ?>
                                                        value="inactive"><?php echo app('translator')->get('app.inactive'); ?></option>
                                                <option
                                                        <?php if($pushSettings->status == 'active'): ?> selected <?php endif; ?>
                                                        value="active"><?php echo app('translator')->get('app.active'); ?></option>
                                            </select>
                                        </div>


                                    </div>


                                    <div class="form-actions m-t-20">
                                        <button type="submit" id="save-form"
                                                class="btn btn-success waves-effect waves-light m-r-10">
                                            <?php echo app('translator')->get('app.update'); ?>
                                        </button>
                                        <button type="button" id="send-test-notification"
                                                class="btn btn-primary waves-effect waves-light"><?php echo app('translator')->get('modules.slackSettings.sendTestNotification'); ?></button>
                                        <button type="reset"
                                                class="btn btn-inverse waves-effect waves-light"><?php echo app('translator')->get('app.reset'); ?></button>
                                    </div>

                                    <?php echo Form::close(); ?>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script src="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.js')); ?>"></script>

    <script>

        $('#save-form').click(function () {
            $.easyAjax({
                url: '<?php echo e(route('admin.push-notification-settings.update', ['1'])); ?>',
                container: '#editSlackSettings',
                type: "POST",
                redirect: true,
                file: true
            })
        });
        $('#removeImageButton').change(function () {
            var removeButton;
            if ($(this).is(':checked'))
                removeButton = 'yes';
            else
                removeButton = 'no';

            var img;
            if(removeButton == 'yes'){
                img = '<img src="https://placeholdit.imgix.net/~text?txtsize=25&txt=<?php echo app('translator')->get('modules.slackSettings.uploadSlackLogo'); ?>&w=200&h=150" alt=""/>';
            }
            else{
                img = '<img src="<?php echo e(asset_url('notification-logo/'.$pushSettings->notification_logo)); ?>" alt=""/>'
            }
            $('.thumbnail').html(img);

        });
    </script>
    <script>

        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function () {
            new Switchery($(this)[0], $(this).data());

        });

        $('.change-email-setting').change(function () {
            var id = $(this).data('setting-id');

            if ($(this).is(':checked'))
                var sendSlack = 'yes';
            else
                var sendSlack = 'no';

            var url = '<?php echo e(route('admin.push-notification-settings.updatePushNotification', ':id')); ?>';
            url = url.replace(':id', id);
            $.easyAjax({
                url: url,
                type: "POST",
                data: {'id': id, 'send_push': sendSlack, '_method': 'POST', '_token': '<?php echo e(csrf_token()); ?>'}
            })
        });

        $('#send-test-notification').click(function () {

            var url = '<?php echo e(route('admin.push-notification-settings.sendTestNotification')); ?>';

            $.easyAjax({
                url: url,
                type: "GET",
                success: function (response) {

                }
            })
        });



    </script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/push-settings/index.blade.php ENDPATH**/ ?>