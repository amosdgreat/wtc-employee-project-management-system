<div class="modal-header" id="attendDetail">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" style="display: inline-block;"><i class="icon-clock"></i> <?php echo app('translator')->get('app.menu.attendance'); ?>
        <?php if($type == 'edit'): ?>
            <?php echo app('translator')->get('app.details'); ?>
        <?php else: ?>
            <?php echo app('translator')->get('app.mark'); ?>
        <?php endif; ?>

    </h4>
    <div class=""  style="display: inline-block;">
        <?php if($clock_in > 0): ?>
            <label class="label label-success"><i class="fa fa-check"></i> <?php echo app('translator')->get('modules.attendance.present'); ?></label>
            <button type="button" title="Attendance Detail" id="viewAttendance" class="btn btn-info btn-sm btn-rounded view-attendance" data-attendance-id="<?php echo e($row->id); ?>">
                <i class="fa fa-search"></i> Detail
            </button>
        <?php else: ?>
            <label class="label label-danger"><i class="fa fa-exclamation-circle"></i> <?php echo app('translator')->get('modules.attendance.absent'); ?></label>
        <?php endif; ?>

    </div>
</div>
<div class="modal-body">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="row">
                            <?php if($total_clock_in < $maxAttandenceInDay): ?>
                                <?php echo Form::open(['id'=>'attendance-container','class'=>'ajax-form','method'=>'POST']); ?>

                                <?php echo e(csrf_field()); ?>

                                <input type="hidden" name="attendance_date" value="<?php echo e($date); ?>">
                                <input type="hidden" name="user_id" value="<?php echo e($userid); ?>">
                                <?php if($type == 'edit'): ?>
                                    <input type="hidden" name="_method" value="PUT">
                                <?php endif; ?>
                                <div class="form-body ">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="input-group bootstrap-timepicker timepicker">
                                                <label><?php echo app('translator')->get('modules.attendance.clock_in'); ?> </label>
                                                <input type="text" name="clock_in_time"
                                                       class="form-control a-timepicker"   autocomplete="off"   id="clock-in-time"
                                                       <?php if(!is_null($row->clock_in_time)): ?> value="<?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->clock_in_time)->timezone($global->timezone)->format($global->time_format)); ?>" <?php endif; ?>>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo app('translator')->get('modules.attendance.clock_in'); ?> IP</label>
                                                <input type="text" name="clock_in_ip" id="clock-in-ip"
                                                       class="form-control" value="<?php echo e($row->clock_in_ip ?? request()->ip()); ?>">
                                            </div>
                                        </div>

                                        <?php if($row->total_clock_in == 0): ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label" ><?php echo app('translator')->get('modules.attendance.late'); ?></label>
                                                    <div class="switchery-demo">
                                                        <input type="checkbox" name="late" <?php if($row->late == "yes"): ?> checked <?php endif; ?> class="js-switch change-module-setting" data-color="#ed4040" id="late"  />
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                    </div>

                                    <div class="row m-t-15">

                                        <div class="col-md-4">
                                            <div class="input-group bootstrap-timepicker timepicker">
                                                <label><?php echo app('translator')->get('modules.attendance.clock_out'); ?></label>
                                                <input type="text" name="clock_out_time" id="clock-out"
                                                       class="form-control b-timepicker"   autocomplete="off"
                                                       <?php if(!is_null($row->clock_out_time)): ?> value="<?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->clock_out_time)->timezone($global->timezone)->format($global->time_format)); ?>" <?php endif; ?>>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo app('translator')->get('modules.attendance.clock_out'); ?> IP</label>
                                                <input type="text" name="clock_out_ip" id="clock-out-ip-<?php echo e($row->id); ?>"
                                                       class="form-control" value="<?php echo e($row->clock_out_ip ?? request()->ip()); ?>">
                                            </div>
                                        </div>

                                        <?php if($row->total_clock_in == 0): ?>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label" ><?php echo app('translator')->get('modules.attendance.halfDay'); ?></label>
                                                    <div class="switchery-demo">
                                                        <input type="checkbox" name="halfday"  <?php if($row->half_day == "yes"): ?> checked <?php endif; ?> class="js-switch change-module-setting" data-color="#ed4040" id="halfday"  />
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo app('translator')->get('modules.attendance.working_from'); ?></label>
                                                <input type="text" name="working_from" id="working-from"
                                                       class="form-control" value="<?php echo e($row->working_from ?? 'office'); ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-success text-white save-attendance"><i
                                                            class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                                <?php echo Form::close(); ?>

                            <?php else: ?>
                                <div class="col-xs-12">
                                    <div class="alert alert-info"><?php echo app('translator')->get('modules.attendance.maxColckIn'); ?></div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $('.a-timepicker').timepicker({
        <?php if($global->time_format == 'H:i'): ?>
        showMeridian: false,
        <?php endif; ?>
        minuteStep: 1
    });
    $('.b-timepicker').timepicker({
        <?php if($global->time_format == 'H:i'): ?>
        showMeridian: false,
        <?php endif; ?>
        minuteStep: 1
    });
    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());

    });
    $('#attendance-container').on('click', '.save-attendance', function () {
        <?php if($type == 'edit'): ?>
            var url = '<?php echo e(route('admin.attendances.update', $row->id)); ?>';
        <?php else: ?>
            var url = '<?php echo e(route('admin.attendances.storeMark')); ?>';
        <?php endif; ?>
        $.easyAjax({
            url: url,
            type: "POST",
            container: '#attendance-container',
            data: $('#attendance-container').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    showTable();
                    $('#projectTimerModal').modal('hide');
                }
            }
        })
    });
    $('#viewAttendance').on('click',function () {
        $('#attendanceModal').modal('hide');
        var attendanceID = $(this).data('attendance-id');
        var url = '<?php echo route('admin.attendances.info', ':attendanceID'); ?>';
        url = url.replace(':attendanceID', attendanceID);

        $('#modelHeading').html('<?php echo e(__("app.menu.attendance")); ?>');
        $.ajaxModal('#projectTimerModal', url);
    });

</script><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/attendance/attendance_mark.blade.php ENDPATH**/ ?>