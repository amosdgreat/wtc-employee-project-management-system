<?php $__env->startSection('other-section'); ?>
<ul class="nav tabs-vertical">

    <li class="tab">
        <a href="<?php echo e(route('admin.settings.index')); ?>"><i class="ti-arrow-left"></i> <?php echo app('translator')->get('app.menu.settings'); ?></a></li>
    <?php if(isset($menuInnerSettingMenu['children'])): ?>

        <?php $__currentLoopData = $menuInnerSettingMenu['children']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php
                $panel = strtok($menu['menu_name'], 'M')
            ?>
            <li class="tab <?php if($type == $panel): ?> active <?php endif; ?>">
                <a href="<?php echo e(route($menu['route'])); ?><?php echo e($panel !== 'admin' ? '?type='.strtok($menu['menu_name'], 'M') : ''); ?>"><?php echo app('translator')->get($menu['translate_name']); ?></a></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <?php if(count($menuInnerSettingMenu) > 0): ?>
            <?php
                $panel = strtok($menuInnerSettingMenu['menu_name'], 'M')
            ?>
            <li class="tab <?php if($type == $panel): ?> active <?php endif; ?>">
                <a href="<?php echo e(route($menuInnerSettingMenu['route'])); ?>?type=<?php echo e(strtok($menuInnerSettingMenu['menu_name'], 'M')); ?>"><?php echo app('translator')->get($menuInnerSettingMenu['translate_name']); ?></a></li>
         <?php endif; ?>
    <?php endif; ?>
</ul>

<script src="<?php echo e(asset('plugins/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
<script>
    var screenWidth = $(window).width();
    if(screenWidth <= 768){

        $('.tabs-vertical').each(function() {
            var list = $(this), select = $(document.createElement('select')).insertBefore($(this).hide()).addClass('settings_dropdown form-control');

            $('>li a', this).each(function() {
                var target = $(this).attr('target'),
                    option = $(document.createElement('option'))
                        .appendTo(select)
                        .val(this.href)
                        .html($(this).html())
                        .click(function(){
                            if(target==='_blank') {
                                window.open($(this).val());
                            }
                            else {
                                window.location.href = $(this).val();
                            }
                        });

                if(window.location.href == option.val()){
                    option.attr('selected', 'selected');
                }
            });
            list.remove();
        });

        $('.settings_dropdown').change(function () {
            window.location.href = $(this).val();
        })

    }
</script>
<?php $__env->stopSection(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/sections/module_setting_menu.blade.php ENDPATH**/ ?>