<?php $__empty_1 = true; $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
<div class="row b-b m-b-5 font-12">
    <div class="col-xs-12">
        <h5><?php echo e(ucwords($comment->user->name)); ?>

            <span class="text-muted font-12"><?php echo e(ucfirst($comment->created_at->diffForHumans())); ?></span>
        </h5>
    </div>
    <div class="col-xs-10">
        <?php echo ucfirst($comment->comment); ?>

    </div>
    <div class="col-xs-2 text-right">
        <a href="javascript:;" data-comment-id="<?php echo e($comment->id); ?>" class="text-danger" onclick="deleteComment('<?php echo e($comment->id); ?>');return false;"><?php echo app('translator')->get('app.delete'); ?></a>
    </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
<div class="col-xs-12">
    <?php echo app('translator')->get('messages.noRecordFound'); ?>
</div>
<?php endif; ?>
<?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/tasks/task_comment.blade.php ENDPATH**/ ?>