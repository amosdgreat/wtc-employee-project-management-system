<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?> #<?php echo e($lead->id); ?> - <span class="font-bold"><?php echo e(ucwords($lead->company_name)); ?></span></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>">Home</a></li>
                <li><a href="<?php echo e(route('admin.leads.index')); ?>"><?php echo e($pageTitle); ?></a></li>
                <li class="active"><?php echo app('translator')->get('modules.lead.followUp'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/icheck/skins/all.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/multiselect/css/multi-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">

            <section>
                <div class="sttabs tabs-style-line">
                    <div class="white-box">

                        <nav>
                            <ul>
                                <li ><a href="<?php echo e(route('admin.leads.show', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.profile'); ?></span></a>
                                </li>
                                <li ><a href="<?php echo e(route('admin.proposals.show', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.proposal'); ?></span></a></li>
                                <li ><a href="<?php echo e(route('admin.lead-files.show', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.file'); ?></span></a></li>

                                <li class="tab-current"><a href="<?php echo e(route('admin.leads.followup', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.followUp'); ?></span></a></li>
                                <?php if($gdpr->enable_gdpr): ?>
                                    <li><a href="<?php echo e(route('admin.leads.gdpr', $lead->id)); ?>"><span>GDPR</span></a></li>
                                <?php endif; ?>
                            </ul>
                        </nav>
                    </div>
                    <div class="content-wrap">
                        <section id="section-line-3" class="show">
                            <div class="row">
                                <div class="col-md-12" id="follow-list-panel">
                                    <div class="white-box">

                                        <div class="row m-b-10">
                                            <div class="col-md-5">
                                                <?php if($lead->next_follow_up == 'yes'): ?>
                                                <a href="javascript:;" id="show-new-follow-panel"
                                                   class="btn btn-success btn-outline"><i class="fa fa-plus"></i> <?php echo app('translator')->get('modules.followup.newFollowUp'); ?></a>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-4 text-right">
                                                <select class="selectpicker" data-style="form-control" id="sort-task" data-lead-id="<?php echo e($lead->id); ?>">
                                                    <option value="id"><?php echo app('translator')->get('modules.tasks.lastCreated'); ?></option>
                                                    <option value="next_follow_up_date"><?php echo app('translator')->get('modules.tasks.dueSoon'); ?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="list-group" id="listGroup" style="max-height: 400px; overflow-y: auto;">
                                            <?php $__empty_1 = true; $__currentLoopData = $lead->follow; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $follow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <a href="javascript:;" data-follow-id="<?php echo e($follow->id); ?>" class="list-group-item edit-task">
                                                    <h4 class="list-group-item-heading sbold"><?php echo app('translator')->get('app.createdOn'); ?>: <?php echo e($follow->created_at->format($global->date_format)); ?></h4>
                                                    <p class="list-group-item-text">
                                                    <div class="row margin-top-5">
                                                        <div class="col-md-12">
                                                            <?php echo app('translator')->get('app.remark'); ?>: <br>
                                                            <?php echo ($follow->remark != '') ? ucfirst($follow->remark) : "<span class='font-red'>Empty</span>"; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row margin-top-5">
                                                        <div class="col-md-6">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php echo app('translator')->get('app.next_follow_up'); ?>: <?php echo e($follow->next_follow_up_date->format($global->date_format)); ?>

                                                        </div>
                                                    </div>
                                                    </p>
                                                </a>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <a href="javascript:;" class="list-group-item">
                                                    <h4 class="list-group-item-heading sbold"><?php echo app('translator')->get('modules.followup.followUpNotFound'); ?></h4>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                        <span class="help-block"><b><?php echo app('translator')->get('app.notice'); ?>: </b>  <?php echo app('translator')->get('modules.followup.followUpNote'); ?></span>
                                    </div>
                                </div>

                                <div class="col-md-4 hide" id="new-follow-panel">
                                    <div class="panel panel-default">
                                        <div class="panel-heading "><i class="ti-plus"></i> <?php echo app('translator')->get('modules.followup.newFollowUp'); ?>
                                            <div class="panel-action">
                                                <a href="javascript:;" id="hide-new-follow-panel"><i class="ti-close"></i></a>
                                            </div>
                                        </div>
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">
                                                <?php echo Form::open(['id'=>'createFollow','class'=>'ajax-form','method'=>'POST']); ?>


                                                <?php echo Form::hidden('lead_id', $lead->id); ?>


                                                <div class="form-body">
                                                    <div class="row">
                                                        <!--/span-->
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label"><?php echo app('translator')->get('app.next_follow_up'); ?></label>
                                                                <input type="text" name="next_follow_up_date" autocomplete="off" id="next_follow_up_date"
                                                                       class="form-control">
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <!--/span-->
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label"><?php echo app('translator')->get('app.description'); ?></label>
                                                                <textarea id="remark" name="remark"
                                                                          class="form-control"></textarea>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->

                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" id="save-task" class="btn btn-success"><i
                                                                class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?>
                                                    </button>
                                                </div>
                                                <?php echo Form::close(); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 hide" id="edit-follow-panel">
                                </div>
                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('js/cbpFWTabs.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>

<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script type="text/javascript">
    //    (function () {
    //
    //        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {
    //            new CBPFWTabs(el);
    //        });
    //
    //    })();

    var newTaskpanel = $('#new-follow-panel');
    var taskListPanel = $('#follow-list-panel');
    var editTaskPanel = $('#edit-follow-panel');

    $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });

    //    save new task
    $('#save-task').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.leads.follow-up-store')); ?>',
            container: '#createFollow',
            type: "POST",
            data: $('#createFollow').serialize(),
            formReset: true,
            success: function (data) {
                $('#listGroup').html(data.html);
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            }
        })
    });
    <?php if($lead->next_follow_up == 'yes'): ?>
        //    save new task
        taskListPanel.on('click', '.edit-task', function () {
            var id = $(this).data('follow-id');
            var url = "<?php echo e(route('admin.leads.follow-up-edit', ':id')); ?>";
            url = url.replace(':id', id);

            $.easyAjax({
                url: url,
                type: "GET",
                data: {taskId: id},
                success: function (data) {
                    editTaskPanel.html(data.html);
                    taskListPanel.switchClass("col-md-12", "col-md-8", 1000, "easeInOutQuad");
                    newTaskpanel.addClass('hide').removeClass('show');
                    editTaskPanel.switchClass("hide", "show", 300, "easeInOutQuad");
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                }
            })
        });

    <?php endif; ?>

    //    save new task
    $('#sort-task, #hide-completed-tasks').change(function() {
        var sortBy = $('#sort-task').val();
        var id = $('#sort-task').data('lead-id');

        var url = "<?php echo e(route('admin.leads.follow-up-sort')); ?>";
        var token = "<?php echo e(csrf_token()); ?>";

        $.easyAjax({
            url: url,
            type: "GET",
            data: {'_token': token, leadId: id, sortBy: sortBy},
            success: function (data) {
                $('#listGroup').html(data.html);
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            }
        })
    });

    $('#show-new-follow-panel').click(function () {
//    taskListPanel.switchClass('col-md-12', 'col-md-8', 1000, 'easeInOutQuad');
        taskListPanel.switchClass("col-md-12", "col-md-8", 1000, "easeInOutQuad");
        editTaskPanel.addClass('hide').removeClass('show');
        newTaskpanel.switchClass("hide", "show", 300, "easeInOutQuad");
    });

    $('#hide-new-follow-panel').click(function () {
        newTaskpanel.addClass('hide').removeClass('show');
        taskListPanel.switchClass("col-md-8", "col-md-12", 1000, "easeInOutQuad");
    });

    editTaskPanel.on('click', '#hide-edit-follow-panel', function () {
        editTaskPanel.addClass('hide').removeClass('show');
        taskListPanel.switchClass("col-md-8", "col-md-12", 1000, "easeInOutQuad");
    });

    jQuery('#next_follow_up_date').datepicker({
        format: '<?php echo e($global->date_picker_format); ?>',
        autoclose: true,
        todayHighlight: true
    })

</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/lead/followup/show.blade.php ENDPATH**/ ?>