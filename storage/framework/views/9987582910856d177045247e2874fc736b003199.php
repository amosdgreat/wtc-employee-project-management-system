<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">

                <div class="col-md-1 col-xs-2">
                    <?php echo ($row->image) ? '<img src="'.asset_url('avatar/'.$row->image).'" alt="user" class="img-circle" width="40">' : '<img src="'.asset('img/default-profile-2.png').'" alt="user" class="img-circle" width="40">'; ?>

                </div>
                <div class="col-md-8 col-xs-6">
                    <?php echo e(ucwords($row->name)); ?> <br>
                    <span class="font-light text-muted"><?php echo e(ucfirst($row->designation_name)); ?></span>
                </div>
                <div class="col-md-3 col-xs-4">
                    <?php if($row->clock_in > 0): ?>
                        <label class="label label-success"><i class="fa fa-check"></i> <?php echo app('translator')->get('modules.attendance.present'); ?></label>
                        <button type="button" title="Attendance Detail" class="btn btn-info btn-sm btn-rounded" onclick="attendanceDetail('<?php echo e($row->id); ?>', '<?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d', $row->atte_date)->timezone($global->timezone)->format('Y-m-d')); ?>')">
                            <i class="fa fa-search"></i> <?php echo app('translator')->get('app.details'); ?>
                        </button>
                    <?php else: ?>
                        <label class="label label-danger"><i class="fa fa-exclamation-circle"></i> <?php echo app('translator')->get('modules.attendance.absent'); ?></label>
                    <?php endif; ?>

                </div>
                <div class="clearfix"></div>

            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="row">
                        <?php if($row->total_clock_in < $maxAttandenceInDay): ?>
                        <?php echo Form::open(['id'=>'attendance-container-'.$row->id,'class'=>'ajax-form','method'=>'POST']); ?>

                        <div class="form-body ">
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="input-group form-group bootstrap-timepicker timepicker">
                                        <label class="required"><?php echo app('translator')->get('modules.attendance.clock_in'); ?> </label>
                                        <input type="text" name="clock_in_time"
                                               class="form-control a-timepicker"   autocomplete="off"   id="clock-in-<?php echo e($row->id); ?>"
                                               <?php if(!is_null($row->clock_in_time)): ?> value="<?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->clock_in_time)->timezone($global->timezone)->format($global->time_format)); ?>" <?php endif; ?>>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label required"><?php echo app('translator')->get('modules.attendance.clock_in'); ?> IP</label>
                                        <input type="text" name="clock_in_ip" id="clock-in-ip-<?php echo e($row->id); ?>"
                                               class="form-control" value="<?php echo e($row->clock_in_ip ?? request()->ip()); ?>">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <label><?php echo app('translator')->get('modules.attendance.clock_out'); ?></label>
                                        <input type="text" name="clock_out_time" id="clock-out-<?php echo e($row->id); ?>"
                                                class="form-control b-timepicker"   autocomplete="off"
                                                <?php if(!is_null($row->clock_out_time)): ?> value="<?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->clock_out_time)->timezone($global->timezone)->format($global->time_format)); ?>" <?php endif; ?>>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.attendance.clock_out'); ?> IP</label>
                                        <input type="text" name="clock_out_ip" id="clock-out-ip-<?php echo e($row->id); ?>"
                                                class="form-control" value="<?php echo e($row->clock_out_ip ?? request()->ip()); ?>">
                                    </div>
                                </div>

                                
                            </div>

                            <div class="row">

                                <?php if($row->total_clock_in == 0): ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" ><?php echo app('translator')->get('modules.attendance.late'); ?></label>
                                            <div class="switchery-demo">
                                                <input type="checkbox" <?php if($row->late == "yes"): ?> checked <?php endif; ?> class="js-switch change-module-setting" data-color="#ed4040" id="late-<?php echo e($row->id); ?>"  />
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if($row->total_clock_in == 0): ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" ><?php echo app('translator')->get('modules.attendance.halfDay'); ?></label>
                                                <div class="switchery-demo">
                                                    <input type="checkbox"  <?php if($row->half_day == "yes"): ?> checked <?php endif; ?> class="js-switch change-module-setting" data-color="#ed4040" id="halfday-<?php echo e($row->id); ?>"  />
                                                </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.attendance.working_from'); ?></label>
                                        <input type="text" name="working_from" id="working-from-<?php echo e($row->id); ?>"
                                               class="form-control" value="<?php echo e($row->working_from ?? 'office'); ?>">
                                    </div>
                                </div>

                                <div class="col-md-3 m-t-20">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success btn-sm text-white save-attendance"
                                                data-user-id="<?php echo e($row->id); ?>"><i
                                                    class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>
                                    </div>
                                </div>


                            </div>

                        </div>
                        <?php echo Form::close(); ?>

                        <?php else: ?>
                            <div class="col-xs-12">
                                <div class="alert alert-info"><?php echo app('translator')->get('modules.attendance.maxColckIn'); ?></div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/attendance/attendance_list.blade.php ENDPATH**/ ?>