<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.language-settings.index')); ?>"><?php echo e($pageTitle); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<?php echo $__env->make('sections.admin_setting_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"><?php echo app('translator')->get('app.add'); ?> <?php echo app('translator')->get('app.language'); ?></div>

                 <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <?php echo Form::open(['id'=>'createCurrency','class'=>'ajax-form','method'=>'POST']); ?>

                                <div class="form-group">
                                    <label for="language_name" class="required"><?php echo app('translator')->get('app.language'); ?> <?php echo app('translator')->get('app.name'); ?></label>
                                    <input type="text" class="form-control" id="language_name" name="language_name"
                                        placeholder="Enter Language Name">
                                </div>
                                <div class="form-group">
                                    <label for="language_code" class="required"><?php echo app('translator')->get('app.language_code'); ?></label>
                                    <input type="text" class="form-control" id="language_code" name="language_code"
                                        placeholder="Enter Language Code">
                                </div>
                                <div class="form-group ">
                                    <label for="usd_price"><?php echo app('translator')->get('app.status'); ?> </label>
                                    <select class="form-control" name="status">
                                        <option value="enabled"><?php echo app('translator')->get('app.enabled'); ?></option>
                                        <option value="disabled"><?php echo app('translator')->get('app.disabled'); ?></option>
                                    </select>
                                </div>

                                <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                                    <?php echo app('translator')->get('app.save'); ?>
                                </button>
                                <button type="reset" class="btn btn-inverse waves-effect waves-light"><?php echo app('translator')->get('app.reset'); ?></button>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script>

    // store language
    $('#save-form').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.language-settings.store')); ?>',
            container: '#createCurrency',
            type: "POST",
            redirect: true,
            data: $('#createCurrency').serialize()
        })
    });
</script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/language-settings/create.blade.php ENDPATH**/ ?>