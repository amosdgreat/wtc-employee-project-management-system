<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?> #<?php echo e($lead->id); ?> - <span
                        class="font-bold"><?php echo e(ucwords($lead->company_name)); ?></span></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.leads.index')); ?>"><?php echo e($pageTitle); ?></a></li>
                <li class="active"><?php echo app('translator')->get('modules.projects.files'); ?></li>
            </ol>
        </div>
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12 text-right">
            <a href="<?php echo e(route('admin.leads.edit',$lead->id)); ?>"
               class="btn btn-outline btn-success btn-sm"><?php echo app('translator')->get('modules.lead.edit'); ?>
                <i class="fa fa-edit" aria-hidden="true"></i></a>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>

<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/dropzone-master/dist/dropzone.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">

            <section>
                <div class="sttabs tabs-style-line">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li class="tab-current"><a href="<?php echo e(route('admin.leads.show', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.profile'); ?></span></a>
                                </li>
                                <li><a href="<?php echo e(route('admin.proposals.show', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.proposal'); ?></span></a></li>
                                <li ><a href="<?php echo e(route('admin.lead-files.show', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.file'); ?></span></a></li>
                                <li><a href="<?php echo e(route('admin.leads.followup', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.followUp'); ?></span></a></li>
                                <?php if($gdpr->enable_gdpr): ?>
                                    <li><a href="<?php echo e(route('admin.leads.gdpr', $lead->id)); ?>"><span>GDPR</span></a></li>
                                <?php endif; ?>
                            </ul>
                        </nav>
                    </div>
                    <div class="content-wrap">
                        <section id="section-line-3" class="show">
                            <div class="row">
                                <div class="col-md-12" id="files-list-panel">

                                        <div class="white-box">
                                            <div class="row">
                                                <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.lead.companyName'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e(ucwords($lead->company_name)); ?></p>
                                                </div>
                                                <div class="col-xs-6"> <strong><?php echo app('translator')->get('modules.lead.website'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->website ?? 'NA'); ?></p>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.lead.mobile'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->mobile ?? 'NA'); ?></p>
                                                </div>
                                                <div class="col-xs-6"> <strong><?php echo app('translator')->get('modules.lead.address'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->address ?? 'NA'); ?></p>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-xs-6 b-r" > <strong><?php echo app('translator')->get('modules.lead.clientName'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->client_name ?? 'NA'); ?></p>
                                                </div>
                                                <div class="col-xs-6"> <strong><?php echo app('translator')->get('modules.lead.clientEmail'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->client_email ?? 'NA'); ?></p>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <?php if($lead->source_id != null): ?>
                                                <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.lead.source'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->lead_source->type ?? 'NA'); ?></p>
                                                </div>
                                                <?php endif; ?>
                                                <?php if($lead->status_id != null): ?>
                                                <div class="col-xs-6"> <strong><?php echo app('translator')->get('modules.lead.status'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->lead_status->type ?? 'NA'); ?></p>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-xs-12"> <strong><?php echo app('translator')->get('app.note'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->note ?? 'NA'); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                </div>

                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/lead/show.blade.php ENDPATH**/ ?>