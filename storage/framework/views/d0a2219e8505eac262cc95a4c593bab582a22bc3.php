<div class="white-box">
    <div class="row">
        <div class="col-md-11 p-r-0">
            <nav>
                <ul class="showProjectTabs">
                    <li class="projects">
                        <a href="<?php echo e(route('admin.projects.show', $project->id)); ?>"><i class="icon-grid"></i>
                            <span><?php echo app('translator')->get('modules.projects.overview'); ?></span></a>
                    </li>
                    <?php if(in_array('employees',$modules)): ?>
                    <li class="projectMembers">
                        <a href="<?php echo e(route('admin.project-members.show', $project->id)); ?>"><i class="icon-people"></i>
                            <span><?php echo app('translator')->get('modules.projects.members'); ?></span></a>
                    </li>
                    <?php endif; ?>
                    <li class="projectMilestones">
                        <a href="<?php echo e(route('admin.milestones.show', $project->id)); ?>"><i class="icon-flag"></i>
                            <span><?php echo app('translator')->get('modules.projects.milestones'); ?></span></a>
                    </li>
                    <?php if(in_array('tasks',$modules)): ?>
                    <li class="projectTasks">
                        <a href="<?php echo e(route('admin.tasks.show', $project->id)); ?>"><i class="fa fa-tasks"></i>
                            <span><?php echo app('translator')->get('app.menu.tasks'); ?></span></a>
                    </li>
                    <?php endif; ?>
                    <li class="projectFiles">
                        <a href="<?php echo e(route('admin.files.show', $project->id)); ?>"><i class="ti-files"></i>
                            <span><?php echo app('translator')->get('modules.projects.files'); ?></span></a>
                    </li>
                    <?php if(in_array('invoices',$modules)): ?>
                    <li class="projectInvoices">
                        <a href="<?php echo e(route('admin.invoices.show', $project->id)); ?>"><i class="ti-file"></i>
                            <span><?php echo app('translator')->get('app.menu.invoices'); ?></span></a>
                    </li>
                    <?php endif; ?> <?php if(in_array('timelogs',$modules)): ?>
                    <li class="projectTimelogs">
                        <a href="<?php echo e(route('admin.time-logs.show', $project->id)); ?>"><i class="ti-alarm-clock"></i>
                            <span><?php echo app('translator')->get('app.menu.timeLogs'); ?></span></a>
                    </li>
                    <?php endif; ?>

                    <li class="burndownChart">
                        <a href="<?php echo e(route('admin.projects.burndown-chart', $project->id)); ?>"><i class="icon-graph"></i>
                            <span><?php echo app('translator')->get('modules.projects.burndownChart'); ?></span></a>
                    </li>



                </ul>
            </nav>
        </div>
        <div class="col-md-1 text-center tabs-more">
            <div class="btn-group dropdown m-r-10">
                <button aria-expanded="false" data-toggle="dropdown" class="btn dropdown-toggle waves-effect waves-light" type="button"><i class="ti-more"></i></button>
                <ul role="menu" class="dropdown-menu pull-right">
                  <li><a href="<?php echo e(route('admin.project-expenses.show', $project->id)); ?>"><i class="ti-shopping-cart" aria-hidden="true"></i> <?php echo app('translator')->get('app.menu.expenses'); ?></a></li>
                  <li><a href="<?php echo e(route('admin.project-payments.show', $project->id)); ?>"><i class="fa fa-money" aria-hidden="true"></i> <?php echo app('translator')->get('app.menu.payments'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/projects/show_project_menu.blade.php ENDPATH**/ ?>