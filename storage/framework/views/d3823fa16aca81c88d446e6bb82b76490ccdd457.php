<?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="row b-b m-b-5 font-12">
        <div class="col-xs-12">
            <h5><?php echo e(ucwords($comment->user->name)); ?> <span class="text-muted font-12"><?php echo e(ucfirst($comment->created_at->diffForHumans())); ?></span></h5>
        </div>
        <div class="col-xs-10">
            <?php echo ucfirst($comment->comment); ?>

        </div>
        <?php if($comment->user->id == $user->id || $user->can('delete_tasks')): ?>
            <div class="col-xs-2 text-right">
                <a href="javascript:;" data-comment-id="<?php echo e($comment->id); ?>" class="text-danger" onclick="deleteComment('<?php echo e($comment->id); ?>');return false;"><?php echo app('translator')->get('app.delete'); ?></a>
            </div>
        <?php endif; ?>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/member/all-tasks/task_comment.blade.php ENDPATH**/ ?>