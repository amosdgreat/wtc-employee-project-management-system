<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e($pageTitle); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.css')); ?>">
    <style>
        .sweet-alert {
            width: 50% !important;
        }
    </style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"><?php echo e($pageTitle); ?></div>

                <div class="vtabs customvtab m-t-10">
                    <?php echo $__env->make('sections.notification_settings_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="tab-content">
                        <div id="vhome3" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-6">

                                    <h3 class="box-title m-b-0"><?php echo app('translator')->get("modules.emailSettings.notificationTitle"); ?></h3>

                                    <p class="text-muted m-b-10 font-13">
                                        <?php echo app('translator')->get("modules.emailSettings.notificationSubtitle"); ?>
                                    </p>

                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12 b-t p-t-20">
                                            <?php echo Form::open(['id'=>'editSettings','class'=>'ajax-form form-horizontal','method'=>'PUT']); ?>


                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.userRegistration"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[4]->send_email == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[4]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.employeeAssign"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[5]->send_email == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[5]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.newNotice"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[6]->send_email == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[6]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.taskAssign"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[7]->send_email == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[7]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.expenseAdded"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[0]->send_email == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[0]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.expenseMember"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[1]->send_email == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[1]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.expenseStatus"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[2]->send_email == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[2]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.ticketRequest"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[3]->send_email == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[3]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.leaveRequest"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[8]->send_email == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[8]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.taskComplete"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[9]->send_email == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[9]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-8"><?php echo app('translator')->get("modules.emailSettings.invoiceNotification"); ?></label>

                                                <div class="col-sm-4">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox"
                                                               <?php if($emailSetting[10]->send_email == 'yes'): ?> checked
                                                               <?php endif; ?> class="js-switch change-email-setting"
                                                               data-color="#99d683"
                                                               data-setting-id="<?php echo e($emailSetting[10]->id); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php echo Form::close(); ?>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">


                                    <h3 class="box-title m-b-0">SMTP <?php echo app('translator')->get("modules.emailSettings.configTitle"); ?></h3>


                                    <p class="text-muted m-b-10 font-13">
                                        &nbsp;
                                    </p>


                                    <div class="row" id="smtp-container">
                                        <div class="col-sm-12 col-xs-12 b-t p-t-20">


                                            <?php echo Form::open(['id'=>'updateSettings','class'=>'ajax-form','method'=>'POST']); ?>

                                            <?php echo Form::hidden('_token', csrf_token()); ?>

                                            <div id="alert">
                                                <?php if($smtpSetting->mail_driver =='smtp'): ?>
                                                    <?php if($smtpSetting->verified): ?>
                                                        <div class="alert alert-success"><?php echo e(__('messages.smtpSuccess')); ?></div>
                                                    <?php else: ?>
                                                        <div class="alert alert-danger"><?php echo e(__('messages.smtpError')); ?></div>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <label><?php echo app('translator')->get("modules.emailSettings.mailDriver"); ?></label>
                                                        <div class="form-group">
                                                            <label class="radio-inline ">
                                                                <input type="radio"
                                                                    class="checkbox"
                                                                    onchange="getDriverValue(this);"
                                                                    value="mail"
                                                                    <?php if($smtpSetting->mail_driver == 'mail'): ?> checked
                                                                    <?php endif; ?> name="mail_driver">Mail
                                                            </label>
                                                            <label class="radio-inline m-l-10">
                                                                <input type="radio"
                                                                            onchange="getDriverValue(this);"
                                                                            value="smtp"
                                                                            <?php if($smtpSetting->mail_driver == 'smtp'): ?> checked
                                                                            <?php endif; ?> name="mail_driver">SMTP
                                                            </label>


                                                        </div>
                                                    </div>

                                                    <!--/span-->
                                                </div>
                                                <div id="smtp_div">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="required"><?php echo app('translator')->get("modules.emailSettings.mailHost"); ?></label>
                                                                <input type="text" name="mail_host" id="mail_host"
                                                                       class="form-control"
                                                                       value="<?php echo e($smtpSetting->mail_host); ?>">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="required"><?php echo app('translator')->get("modules.emailSettings.mailPort"); ?></label>
                                                                <input type="text" name="mail_port" id="mail_port"
                                                                       class="form-control"
                                                                       value="<?php echo e($smtpSetting->mail_port); ?>">
                                                            </div>
                                                        </div>
                                                        <!--/span-->

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="required"><?php echo app('translator')->get("modules.emailSettings.mailUsername"); ?></label>
                                                                <input type="text" name="mail_username"
                                                                       id="mail_username"
                                                                       class="form-control"
                                                                       value="<?php echo e($smtpSetting->mail_username); ?>">
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label required"><?php echo app('translator')->get("modules.emailSettings.mailPassword"); ?></label>
                                                                <input type="password" name="mail_password"
                                                                       id="mail_password"
                                                                       class="form-control"
                                                                       value="<?php echo e($smtpSetting->mail_password); ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label required"><?php echo app('translator')->get("modules.emailSettings.mailEncryption"); ?></label>
                                                                <select class="form-control" name="mail_encryption"
                                                                        id="mail_encryption">
                                                                    <option <?php if($smtpSetting->mail_encryption == 'tls'): ?> selected <?php endif; ?>>
                                                                        tls
                                                                    </option>
                                                                    <option <?php if($smtpSetting->mail_encryption == 'ssl'): ?> selected <?php endif; ?>>
                                                                        ssl
                                                                    </option>

                                                                    <option value="null"
                                                                            <?php if($smtpSetting->mail_encryption == null): ?> selected <?php endif; ?>>
                                                                        none
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label required"><?php echo app('translator')->get("modules.emailSettings.mailFrom"); ?></label>
                                                        <input type="text" name="mail_from_name"
                                                               id="mail_from_name"
                                                               class="form-control"
                                                               value="<?php echo e($smtpSetting->mail_from_name); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label required"><?php echo app('translator')->get("modules.emailSettings.mailFromEmail"); ?></label>
                                                        <input type="text" name="mail_from_email"
                                                               id="mail_from_email"
                                                               class="form-control"
                                                               value="<?php echo e($smtpSetting->mail_from_email); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->


                                            <div class="form-actions">
                                                <button type="submit" id="save-form" class="btn btn-success"><i
                                                            class="fa fa-check"></i>
                                                    <?php echo app('translator')->get('app.update'); ?>
                                                </button>
                                                <button type="button" id="send-test-email"
                                                        class="btn btn-primary"><?php echo app('translator')->get('modules.emailSettings.sendTestEmail'); ?></button>
                                                <button type="reset" class="btn btn-default"><?php echo app('translator')->get('app.reset'); ?></button>
                                            </div>
                                            <?php echo Form::close(); ?>

                                        </div>
                                    </div>

                                </div>
                                <!-- .row -->

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
        <!-- .row -->

        
        <div class="modal fade bs-modal-md in" id="testMailModal" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-md" id="modal-data-application">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Test Email</h4>
                    </div>
                    <div class="modal-body">
                        <?php echo Form::open(['id'=>'testEmail','class'=>'ajax-form','method'=>'POST']); ?>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Enter email address where test mail needs to be sent</label>
                                        <input type="text" name="test_email" id="test_email"
                                               class="form-control"
                                               value="<?php echo e($user->email); ?>">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                        </div>
                        <div class="form-actions">
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success" id="send-test-email-submit">submit</button>
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->.
            </div>

        </div> 
    </div> 

<?php $__env->stopSection(); ?>
<?php $__env->startPush('footer-script'); ?>
    <script src="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.js')); ?>"></script>
    <script>

        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function () {
            new Switchery($(this)[0], $(this).data());

        });

        $('.change-email-setting').change(function () {
            var id = $(this).data('setting-id');

            if ($(this).is(':checked'))
                var sendEmail = 'yes';
            else
                var sendEmail = 'no';

            var url = '<?php echo e(route('admin.email-settings.update', ':id')); ?>';
            url = url.replace(':id', id);
            $.easyAjax({
                url: url,
                type: "POST",
                data: {'id': id, 'send_email': sendEmail, '_method': 'PUT', '_token': '<?php echo e(csrf_token()); ?>'}
            })
        });

        $('#save-form').click(function () {

            var url = '<?php echo e(route('admin.email-settings.updateMailConfig')); ?>';

            $.easyAjax({
                url: url,
                type: "POST",
                container: '#updateSettings',
                messagePosition: "inline",
                data: $('#updateSettings').serialize(),
                success: function (response) {
                    if (response.status == 'error') {
                        $('#alert').prepend('<div class="alert alert-danger"><?php echo e(__('messages.smtpError')); ?></div>')
                    } else {
                        $('#alert').show();
                        $.showToastr(response.message, 'success','')
                    }
                }
            })
        });

        $('#send-test-email').click(function () {
            $('#testMailModal').modal('show')
        });
        $('#send-test-email-submit').click(function () {
            $.easyAjax({
                url: '<?php echo e(route('admin.email-settings.sendTestEmail')); ?>',
                type: "GET",
                messagePosition: "inline",
                container: "#testEmail",
                data: $('#testEmail').serialize(),

            })
        });


        function getDriverValue(sel) {
            if (sel.value == 'mail') {
                $('#smtp_div').hide();
                $('#alert').hide();
            } else {
                $('#smtp_div').show();
                $('#alert').show();
            }
        }

        <?php if($smtpSetting->mail_driver == 'mail'): ?>
        $('#smtp_div').hide();
        $('#alert').hide();
        <?php endif; ?>
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/email-settings/index.blade.php ENDPATH**/ ?>