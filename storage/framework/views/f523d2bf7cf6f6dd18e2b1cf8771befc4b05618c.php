<?php $__env->startSection('page-title'); ?>
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
            <li class="active"><?php echo e($pageTitle); ?></li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/dropzone-master/dist/dropzone.css')); ?>">
<style>
    .file-bg {
        height: 150px;
        overflow: hidden;
        position: relative;
    }

    .file-bg .overlay-file-box {
        opacity: .9;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        height: 100%;
        text-align: center;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>


<div class="row">
    <?php ($envatoUpdateCompanySetting = \Froiden\Envato\Functions\EnvatoUpdate::companySetting()); ?>
        <?php if(!is_null($envatoUpdateCompanySetting->supported_until)): ?>
            <div class="col-md-12" id="support-div">
                <?php if(\Carbon\Carbon::parse($envatoUpdateCompanySetting->supported_until)->isPast()): ?>
                    <div class="col-md-12 alert alert-danger ">
                        <div class="col-md-6">
                            Your support has been expired on <b><span
                                        id="support-date"><?php echo e(\Carbon\Carbon::parse($envatoUpdateCompanySetting->supported_until)->format('d M, Y')); ?></span></b>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?php echo e(config('froiden_envato.envato_product_url')); ?>" target="_blank"
                            class="btn btn-inverse btn-small">Renew support <i class="fa fa-shopping-cart"></i></a>
                            <a href="javascript:;" onclick="getPurchaseData();" class="btn btn-inverse btn-small">Refresh
                                <i
                                        class="fa fa-refresh"></i></a>
                        </div>
                    </div>

                <?php else: ?>
                    <div class="col-md-12 alert alert-info">
                        Your support will expire on <b><span
                                    id="support-date"><?php echo e(\Carbon\Carbon::parse($envatoUpdateCompanySetting->supported_until)->format('d M, Y')); ?></span></b>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading"><?php echo e($pageTitle); ?></div>

            <div class="vtabs customvtab m-t-10">

                <?php echo $__env->make('sections.admin_setting_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


                <div class="tab-content">

                    
                    
                    <div id="vhome3" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="<?php echo e(route('admin.update-settings.index')); ?>" class="btn btn-warning btn-sm btn-outline "><i class="fa fa-arrow-left"></i> <?php echo app('translator')->get('app.back'); ?></a>
                                </div>
                            </div>

                            <?php if(!is_null($envatoUpdateCompanySetting->supported_until) && !\Carbon\Carbon::parse($envatoUpdateCompanySetting->supported_until)->isPast()): ?>

                                <?php if(isset($lastVersion)): ?>
                                <!--row-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="box-title" id="structure"><?php echo app('translator')->get('modules.update.updateManual'); ?></h4>
                                    </div>

                                    <div class="col-md-12">
                                        <h4 class="box-title text-info">Step 1</h4>
                                        <a href="https://updates.froid.works/Wtcepms/download.php?<?php echo e($encryptedDownloadLink); ?>" target="_blank" class="btn btn-success btn-sm btn-outline"><?php echo app('translator')->get('modules.update.downloadUpdateFile'); ?> <i class="fa fa-download"></i></a>
                                    </div>

                                    <div class="col-md-12 m-t-20">
                                            <h4 class="box-title text-info">Step 2</h4>
                                            <form action="<?php echo e(route('admin.update-settings.store')); ?>" class="dropzone" id="file-upload-dropzone">
                                            <?php echo e(csrf_field()); ?>


                                            <div class="fallback">
                                                <input name="file" type="file" multiple />
                                            </div>
                                        </form>
                                    </div>

                                    <div class="col-md-12 m-t-20" id="install-process">
                                        
                                    </div>

                                    <div class="col-md-12 m-t-20">
                                        <h4 class="box-title text-info">Step 3</h4>
                                        <h4 class="box-title"><?php echo app('translator')->get('modules.update.updateFiles'); ?></h4>
                                    </div>
                                    <div class="col-md-12">
                                        <ul class="list-group" id="files-list">
                                            <?php $__currentLoopData = \Illuminate\Support\Facades\File::files($updateFilePath); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$filename): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(\Illuminate\Support\Facades\File::basename($filename) != "modules_statuses.json"): ?>
                                                <li class="list-group-item" id="file-<?php echo e($key+1); ?>">
                                                        <div class="row">
                                                            <div class="col-md-9">
                                                                <?php echo e(\Illuminate\Support\Facades\File::basename($filename)); ?>

                                                            </div>
                                                            <div class="col-md-3 text-right">
                                                                <button type="button" class="btn btn-success btn-sm btn-outline install-files" data-file-no="<?php echo e($key+1); ?>" data-file-path="<?php echo e($filename); ?>"><?php echo app('translator')->get('modules.update.install'); ?> <i class="fa fa-refresh"></i></button>

                                                                <button type="button" class="btn btn-danger btn-sm btn-outline delete-files" data-file-no="<?php echo e($key+1); ?>" data-file-path="<?php echo e($filename); ?>"><?php echo app('translator')->get('app.delete'); ?> <i class="fa fa-times"></i></button>
                                                            </div>
                                                        </div>
                                                    </li>
                                            <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>



                                </div>
                                <!--/row-->
                                <?php else: ?>
                                    <div class="row">
                                        <div class="col-md-12 m-t-20">
                                            <div class="alert alert-success ">
                                                    You have latest version of this app.
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>

                    </div>
                </div>


            </div>
        </div>


    </div>
    <!-- .row -->

    <?php $__env->stopSection(); ?>

    <?php $__env->startPush('footer-script'); ?>
    <script src="<?php echo e(asset('plugins/bower_components/dropzone-master/dist/dropzone.js')); ?>"></script>
    <script type="text/javascript">
        // "myAwesomeDropzone" is the camelized version of the HTML element's ID
        Dropzone.options.fileUploadDropzone = {
            paramName: "file", // The name that will be used to transfer the file
    //        maxFilesize: 2, // MB,
            dictDefaultMessage: "Upload or drop the downloaded file here",
            accept: function (file, done) {
                done();
            },
            init: function () {
                this.on("success", function (file, response) {
                    var viewName = $('#view').val();
                    if(viewName == 'list') {
                        $('#files-list-panel ul.list-group').html(response.html);
                    } else {
                        $('#thumbnail').empty();
                        $(response.html).hide().appendTo("#thumbnail").fadeIn(500);
                    }
                })
            }
        };



        var updateAreaDiv = $('#update-area');
        var refreshPercent = 0;
        var checkInstall = true;

        function checkIfFileExtracted(){
            $.easyAjax({
                type: 'GET',
                url: '<?php echo route("admin.updateVersion.checkIfFileExtracted"); ?>',
                success: function (response) {
                    checkInstall = false;
                    $('#download-progress').append("<br><i><span class='text-success'>Installed successfully. Reload page to see the changes.</span>.</i>");
                    document.getElementById('logout-form').submit();
                }
            });
        }

        $('.install-files').click(function(){
            $('#install-process').html('<div class="alert alert-info ">Installing...Please wait (This may take few minutes.)</div>');
            window.setInterval(function(){
                /// call your function here
                if(checkInstall == true){
                    checkIfFileExtracted();
                    console.log("install check");
                }
            }, 1500);
            console.log("install start");
            
            let filePath = $(this).data('file-path');
            $.easyAjax({
                type: 'GET',
                url: '<?php echo route("admin.update-settings.install"); ?>',
                data: {filePath: filePath},
                success: function (response) {
                    $('#download-progress').append("<br><i><span class='text-success'>Installed successfully. Reload page to see the changes.</span>.</i>");
                    document.getElementById('logout-form').submit();
                }
            });
        });

        $('.delete-files').click(function(){
            let filePath = $(this).data('file-path');
            let fileNumber = $(this).data('file-no');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                   
                    $.easyAjax({
                        type: 'POST',
                        url: '<?php echo route("admin.update-settings.deleteFile"); ?>',
                        data: {"_token": "<?php echo e(csrf_token()); ?>", filePath: filePath},
                        success: function (response) {
                            $('#file-'+fileNumber).remove();
                        }
                    });
                }
            });
        });
    </script>
    <?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/update-database/manual.blade.php ENDPATH**/ ?>