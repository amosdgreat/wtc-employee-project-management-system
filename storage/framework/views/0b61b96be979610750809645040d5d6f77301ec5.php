<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title"><?php echo app('translator')->get('modules.accountSettings.currencyConverterKey'); ?></h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        <div class="alert alert-info ">
            <i class="fa fa-info-circle"></i> <?php echo app('translator')->get('messages.currencyConvertApiKeyUrl'); ?> <a href="https://www.currencyconverterapi.com" target="_blank" class="text-white"> https://www.currencyconverterapi.com</a>
        </div>
        <?php echo Form::open(['id'=>'createCurrencyKey','class'=>'ajax-form','method'=>'POST']); ?>

        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label><?php echo app('translator')->get('modules.accountSettings.currencyConverterKey'); ?></label>
                        <input type="password" name="currency_converter_key" id="currency_converter_key" value="<?php echo e($global->currency_converter_key); ?>" class="form-control">
                        <span class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>
        </div>
        <?php echo Form::close(); ?>

    </div>
</div>

<script>

    $('#save-category').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.currency.exchange-key-store')); ?>',
            container: '#createCurrencyKey',
            type: "POST",
            data: $('#createCurrencyKey').serialize(),
            success: function (response) {
              $('#projectCategoryModal').modal('hide');
            }
        })
    });
</script>
<?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/currencies/currency_exchange_key.blade.php ENDPATH**/ ?>