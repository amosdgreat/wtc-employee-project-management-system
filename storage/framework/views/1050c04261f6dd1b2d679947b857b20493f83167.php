<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?> <span class="text-warning b-l p-l-10 m-l-5"><?php echo e(count($pendingLeaves)); ?></span> <span class="font-12 text-muted m-l-5"> <?php echo app('translator')->get('modules.leaves.pendingLeaves'); ?></span></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <?php if(!$pendingLeaves->isEmpty()): ?>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
            <a href="<?php echo e(route('admin.leave.all-leaves')); ?>" class="btn btn-sm btn-info waves-effect waves-light btn-outline">
                        <i class="fa fa-list"></i> <?php echo app('translator')->get('app.all'); ?> <?php echo app('translator')->get('app.menu.leaves'); ?>
            </a>

            <a href="<?php echo e(route('admin.leaves.index')); ?>" class="btn btn-sm btn-primary waves-effect waves-light m-l-10 btn-outline">
                    <i class="fa fa-calendar"></i> <?php echo app('translator')->get('modules.leaves.calendarView'); ?>
            </a>

            <a href="<?php echo e(route('admin.leaves.create')); ?>" class="btn btn-sm btn-success waves-effect waves-light m-l-10 btn-outline">
            <i class="ti-plus"></i> <?php echo app('translator')->get('modules.leaves.assignLeave'); ?></a>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e($pageTitle); ?></li>
            </ol>

        </div>
    <?php endif; ?>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">

        <div class="col-md-12">

            <div class="white-box">
                <div class="row">
                    <?php $__empty_1 = true; $__currentLoopData = $pendingLeaves; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$pendingLeave): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <div class="col-md-3 m-b-25">
                            <div class=" pending-leaves  p-10">
                            <h5 class="font-normal"><?php echo e($pendingLeave->type->type_name); ?> <?php echo app('translator')->get('modules.leaves.leaveRequest'); ?> <?php if($pendingLeave->duration == 'half day'): ?> <label class="label label-danger"><?php echo app('translator')->get('modules.leaves.halfDay'); ?></label> <?php endif; ?></h5>
                            <div class="m-b-15">
                                <img src="<?php echo e($pendingLeave->user->image_url); ?>" alt="user" class="img-circle" width="30" height="30" height="30">
                                <span class="m-l-5"><a href="<?php echo e(route('admin.employees.show', $pendingLeave->user_id)); ?>" ><?php echo e(ucwords($pendingLeave->user->name)); ?></a></span>
                            </div>
                            <?php
                                $leavesRemaining = ($allowedLeaves-$pendingLeave->leaves_taken_count);
                                $percentLeavesRemaining = ($leavesRemaining/$allowedLeaves) * 100;
                            ?>
                            <div class="text-center bg-light p-t-20 p-b-20 m-l-n-25 m-r-n-25">
                                <?php echo e($pendingLeave->leave_date->format($global->date_format)); ?> (<?php echo e($pendingLeave->leave_date->format('l')); ?>)
                                <div class="progress m-l-30 m-r-30 m-t-15">
                                    <div class="progress-bar progress-bar-info" aria-valuenow="<?php echo e($percentLeavesRemaining); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo e($percentLeavesRemaining); ?>%" role="progressbar"> <span class="sr-only"><?php echo e($percentLeavesRemaining); ?>% Complete</span> </div>
                                </div>

                                <div class="m-l-30 m-r-30 m-t-15">
                                    <?php echo e(($leavesRemaining)); ?> <?php echo app('translator')->get('modules.leaves.remainingLeaves'); ?>
                                </div>
                            </div>

                            <div class="p-t-10">
                                <h6 class="font-normal"><?php echo app('translator')->get('app.reason'); ?></h6>
                                <div class="p-b-15 font-12" style="height: 80px; overflow-y: auto;"><?php echo e($pendingLeave->reason); ?></div>

                                <div class="p-t-20 text-center m-l-n-25 m-r-n-25">
                                    <a href="javascript:;" data-leave-id="<?php echo e($pendingLeave->id); ?>" data-leave-action="approved" class="btn btn-success btn-rounded m-r-5 leave-action"><i class="fa fa-check"></i> <?php echo app('translator')->get('app.accept'); ?></a>

                                    <a href="javascript:;" data-leave-id="<?php echo e($pendingLeave->id); ?>" data-leave-action="rejected" class="btn btn-danger btn-rounded leave-action-reject"><i class="fa fa-times"></i> <?php echo app('translator')->get('app.reject'); ?></a>

                                </div>

                            </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <div  class="text-center">
                            <div class="empty-space" style="height: 300px;">
                                <div class="empty-space-inner">
                                    <div class="icon" style="font-size:30px"><i
                                                class="icon-logout"></i>
                                    </div>
                                    <div class="title m-b-15"><?php echo app('translator')->get('messages.noPendingLeaves'); ?>
                                    </div>
                                    <div class="subtitle">
                                        <a href="<?php echo e(route('admin.leave.all-leaves')); ?>" class="btn btn-sm btn-info waves-effect waves-light btn-outline">
                                            <i class="fa fa-list"></i> <?php echo app('translator')->get('app.all'); ?> <?php echo app('translator')->get('app.menu.leaves'); ?>
                                        </a>

                                        <a href="<?php echo e(route('admin.leaves.index')); ?>" class="btn btn-sm btn-primary waves-effect waves-light m-l-10 btn-outline">
                                            <i class="fa fa-calendar"></i> <?php echo app('translator')->get('modules.leaves.calendarView'); ?>
                                        </a>

                                        <a href="<?php echo e(route('admin.leaves.create')); ?>" class="btn btn-sm btn-success waves-effect waves-light m-l-10 btn-outline">
                                            <i class="ti-plus"></i> <?php echo app('translator')->get('modules.leaves.assignLeave'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>


    </div>
    <!-- .row -->

    
    <div class="modal fade bs-modal-md in" id="eventDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>

<script>


    $('.leave-action-reject').click(function () {
        var action = $(this).data('leave-action');
        var leaveId = $(this).data('leave-id');
        var searchQuery = "?leave_action="+action+"&leave_id="+leaveId;
        var url = '<?php echo route('admin.leaves.show-reject-modal'); ?>'+searchQuery;
        $('#modelHeading').html('Reject Reason');
        $.ajaxModal('#eventDetailModal', url);
    });

    $('.leave-action').on('click', function() {
        var action = $(this).data('leave-action');
        var leaveId = $(this).data('leave-id');
        var url = '<?php echo e(route("admin.leaves.leaveAction")); ?>';

        $.easyAjax({
            type: 'POST',
            url: url,
            data: { 'action': action, 'leaveId': leaveId, '_token': '<?php echo e(csrf_token()); ?>' },
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        });
    })
</script>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/leaves/pending.blade.php ENDPATH**/ ?>