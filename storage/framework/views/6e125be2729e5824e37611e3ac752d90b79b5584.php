<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.currency.index')); ?>"><?php echo e($pageTitle); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('sections.admin_setting_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('modules.currencySettings.addNewCurrency'); ?></div>

                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <?php echo Form::open(['id'=>'createCurrency','class'=>'ajax-form','method'=>'POST']); ?>

                                <div class="form-group">
                                    <label for="company_name" class="required"><?php echo app('translator')->get('modules.currencySettings.currencyName'); ?></label>
                                    <input type="text" class="form-control" id="currency_name" name="currency_name"
                                        placeholder="Enter Currency Name">
                                </div>
                                <div class="form-group">
                                    <label><?php echo app('translator')->get('modules.currencySettings.isCryptoCurrency'); ?>?</label>
                                    <div class="radio-list">
                                        <label class="radio-inline p-0">
                                            <div class="radio radio-info">
                                                <input type="radio" name="is_cryptocurrency" id="crypto_currency_yes" value="yes">
                                                <label for="crypto_currency_yes"><?php echo app('translator')->get('app.yes'); ?></label>
                                            </div>
                                        </label>
                                        <label class="radio-inline">
                                            <div class="radio radio-info">
                                                <input type="radio" name="is_cryptocurrency" checked id="crypto_currency_no" value="no">
                                                <label for="crypto_currency_no"><?php echo app('translator')->get('app.no'); ?></label>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="company_email" class="required"><?php echo app('translator')->get('modules.currencySettings.currencySymbol'); ?></label>
                                    <input type="text" class="form-control" id="currency_symbol" name="currency_symbol">
                                </div>
                                <div class="form-group">
                                    <label for="company_phone" class="required"><?php echo app('translator')->get('modules.currencySettings.currencyCode'); ?></label>
                                    <input type="text" class="form-control" id="currency_code" name="currency_code" >
                                </div>
                                <div class="form-group crypto-currency" style="display: none">
                                    <label for="usd_price"><?php echo app('translator')->get('modules.currencySettings.usdPrice'); ?> <a class="mytooltip" href="javascript:void(0)"> <i class="fa fa-info-circle"></i><span class="tooltip-content5"><span class="tooltip-text3"><span class="tooltip-inner2"><?php echo app('translator')->get('modules.currencySettings.usdPriceInfo'); ?></span></span></span></a></label>
                                    <input type="text" class="form-control" id="usd_price" name="usd_price" >
                                </div>

                                <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                                    <?php echo app('translator')->get('app.save'); ?>
                                </button>
                                <a href="<?php echo e(route('admin.currency.index')); ?>" class="btn btn-default waves-effect waves-light"><?php echo app('translator')->get('app.back'); ?></a>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script>

    $("input[name=is_cryptocurrency]").click(function () {
        if($(this).val() == 'yes'){
            $('.regular-currency').hide();
            $('.crypto-currency').show();
        }
        else{
            $('.crypto-currency').hide();
            $('.regular-currency').show();
        }
    })


    $('#save-form').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.currency.store')); ?>',
            container: '#createCurrency',
            type: "POST",
            redirect: true,
            data: $('#createCurrency').serialize()
        })
    });
</script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/currencies/create.blade.php ENDPATH**/ ?>