<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.all-invoices.index')); ?>"><?php echo e($pageTitle); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        .dropdown-content {
            width: 250px;
            max-height: 250px;
            overflow-y: scroll;
            overflow-x: hidden;
        }
    </style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('modules.proposal.convertProposalTitle'); ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <?php echo Form::open(['id'=>'storePayments','class'=>'ajax-form','method'=>'POST']); ?>

                        <input type="hidden" name="proposal_id" value="<?php echo e($proposalId); ?>">
                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('app.invoice'); ?> #</label>
                                            <div>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><span class="invoicePrefix" data-prefix="<?php echo e($invoiceSetting->invoice_prefix); ?>"><?php echo e($invoiceSetting->invoice_prefix); ?></span>#<span class="noOfZero" data-zero="<?php echo e($invoiceSetting->invoice_digit); ?>"><?php echo e($zero); ?></span></div>
                                                    <input type="text"  class="form-control readonly-background" name="invoice_number" readonly id="invoice_number" value="<?php if(is_null($lastInvoice)): ?>1 <?php else: ?><?php echo e(($lastInvoice)); ?><?php endif; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('app.client'); ?></label>
                                        <?php if(is_null($invoice->lead->client_id)): ?>
                                            <select class="select2 form-control" data-placeholder="Choose Project"
                                                    name="client_id">
                                                <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option <?php if($invoice->lead->client_id == $client->id): ?> selected  <?php endif; ?>
                                                            value="<?php echo e($client->id); ?>"><?php echo e(ucwords($client->name)); ?>

                                                    </option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        <?php else: ?>
                                            <span class="form-control"><?php echo e(ucwords($invoice->lead->client->name)); ?></span>
                                            <input type="hidden" name="client_id" value="<?php echo e($invoice->lead->client_id); ?>">
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.invoices.currency'); ?></label>
                                        <select class="form-control" name="currency_id" id="currency_id">
                                            <?php $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currency): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option
                                                        <?php if($invoice->currency_id == $currency->id): ?> selected
                                                        <?php endif; ?>
                                                        value="<?php echo e($currency->id); ?>"><?php echo e($currency->currency_symbol.' ('.$currency->currency_code.')'); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>

                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.invoices.invoiceDate'); ?></label>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control" name="issue_date"
                                                           id="invoice_date"
                                                           value="<?php echo e(Carbon\Carbon::today()->format($global->date_format)); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('app.dueDate'); ?></label>

                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="due_date" id="due_date"
                                                   value="<?php echo e($invoice->valid_till->format($global->date_format)); ?>">

                                            <input type="hidden" name="recurring_payment" id="recurring_payment" value="no">
                                            <input type="hidden" name="project_id" id="project_id" value="">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('app.status'); ?></label>
                                        <select class="form-control" name="status" id="status">
                                            <option
                                                    <?php if($invoice->status == 'paid'): ?> selected <?php endif; ?>
                                            value="paid"><?php echo app('translator')->get('modules.invoices.paid'); ?>
                                            </option>
                                            <option
                                                    <?php if($invoice->status == 'unpaid'): ?> selected <?php endif; ?>
                                            value="unpaid"><?php echo app('translator')->get('modules.invoices.unpaid'); ?>
                                            </option>
                                        </select>
                                    </div>

                                </div>

                            </div>
                            <hr>
                            <div class="row">

                                <div class="col-xs-12  visible-md visible-lg">

                                    <div class="col-md-4 font-bold" style="padding: 8px 15px">
                                        <?php echo app('translator')->get('modules.invoices.item'); ?>
                                    </div>

                                    <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                        <?php echo app('translator')->get('modules.invoices.qty'); ?>
                                    </div>

                                    <div class="col-md-2 font-bold" style="padding: 8px 15px">
                                        <?php echo app('translator')->get('modules.invoices.unitPrice'); ?>
                                    </div>

                                    <div class="col-md-2 font-bold" style="padding: 8px 15px">
                                        <?php echo app('translator')->get('modules.invoices.tax'); ?> <a href="javascript:;" id="tax-settings" ><i class="ti-settings text-info"></i></a>
                                    </div>

                                    <div class="col-md-2 text-center font-bold" style="padding: 8px 15px">
                                        <?php echo app('translator')->get('modules.invoices.amount'); ?>
                                    </div>

                                    <div class="col-md-1" style="padding: 8px 15px">
                                        &nbsp;
                                    </div>

                                </div>

                                <div id="sortable">
                                    <?php $__currentLoopData = $invoice->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-xs-12 item-row margin-top-5">
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.item'); ?></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                                                            <input type="text" class="form-control item_name" name="item_name[]"
                                                                   value="<?php echo e($item->item_name); ?>" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea name="item_summary[]" class="form-control" placeholder="<?php echo app('translator')->get('app.description'); ?>" rows="2"><?php echo e($item->item_summary); ?></textarea>

                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-1">

                                                <div class="form-group">
                                                    <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.qty'); ?></label>
                                                    <input type="number" min="1" class="form-control quantity"
                                                           value="<?php echo e($item->quantity); ?>" name="quantity[]"
                                                    >
                                                </div>


                                            </div>

                                            <div class="col-md-2">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.unitPrice'); ?></label>
                                                        <input type="text" min="" class="form-control cost_per_item"
                                                               name="cost_per_item[]" value="<?php echo e(number_format((float)$item->unit_price, 2, '.', '')); ?>"
                                                        >
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-2">

                                                <div class="form-group">
                                                    <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.tax'); ?></label>
                                                    <select id="multiselect" name="taxes[<?php echo e($key); ?>][]"  multiple="multiple" class="selectpicker form-control type">
                                                        <?php $__currentLoopData = $taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option data-rate="<?php echo e($tax->rate_percent); ?>"
                                                                    <?php if(isset($item->taxes) && array_search($tax->id, json_decode($item->taxes)) !== false): ?>
                                                                    selected
                                                                    <?php endif; ?>
                                                                    value="<?php echo e($tax->id); ?>"><?php echo e($tax->tax_name); ?>: <?php echo e($tax->rate_percent); ?>%</option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>


                                            </div>

                                            <div class="col-md-2 border-dark  text-center">
                                                <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.amount'); ?></label>

                                                <p class="form-control-static"><span
                                                            class="amount-html"><?php echo e(number_format((float)$item->amount, 2, '.', '')); ?></span></p>
                                                <input type="hidden" value="<?php echo e($item->amount); ?>" class="amount"
                                                       name="amount[]">
                                            </div>

                                            <div class="col-md-1 text-right visible-md visible-lg">
                                                <button type="button" class="btn remove-item btn-circle btn-danger"><i
                                                            class="fa fa-remove"></i></button>
                                            </div>
                                            <div class="col-xs-12 text-center hidden-md hidden-lg">
                                                <div class="row">
                                                    <button type="button" class="btn btn-circle remove-item btn-danger"><i
                                                                class="fa fa-remove"></i>
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>


                                <div class="col-xs-12 m-t-5">
                                    <button type="button" class="btn btn-info" id="add-item"><i class="fa fa-plus"></i> <?php echo app('translator')->get('modules.invoices.addItem'); ?></button>
                                </div>

                                <div class="col-xs-12 ">


                                        <div class="row">
                                            <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10"><?php echo app('translator')->get('modules.invoices.subTotal'); ?></div>

                                            <p class="form-control-static col-xs-6 col-md-2">
                                                <span class="sub-total"><?php echo e(number_format((float)$invoice->sub_total, 2, '.', '')); ?></span>
                                            </p>


                                            <input type="hidden" class="sub-total-field" name="sub_total" value="<?php echo e($invoice->sub_total); ?>">
                                        </div>

                                        <div class="row">
                                            <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                                <?php echo app('translator')->get('modules.invoices.discount'); ?>
                                            </div>
                                            <div class="form-group col-xs-6 col-md-1" >
                                                <input type="number" min="0" value="<?php echo e($invoice->discount); ?>" name="discount_value" class="form-control discount_value" >
                                            </div>
                                            <div class="form-group col-xs-6 col-md-1" >
                                                <select class="form-control" name="discount_type" id="discount_type">
                                                    <option
                                                            <?php if($invoice->discount_type == 'percent'): ?> selected <?php endif; ?>
                                                            value="percent">%</option>
                                                    <option
                                                            <?php if($invoice->discount_type == 'fixed'): ?> selected <?php endif; ?>
                                                    value="fixed"><?php echo app('translator')->get('modules.invoices.amount'); ?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row m-t-5" id="invoice-taxes">
                                            <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10">
                                                <?php echo app('translator')->get('modules.invoices.tax'); ?>
                                            </div>

                                            <p class="form-control-static col-xs-6 col-md-2" >
                                                <span class="tax-percent">0</span>
                                            </p>
                                        </div>

                                        <div class="row m-t-5 font-bold">
                                            <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10"><?php echo app('translator')->get('modules.invoices.total'); ?></div>

                                            <p class="form-control-static col-xs-6 col-md-2">
                                                <span class="total"><?php echo e(number_format((float)$invoice->total, 2, '.', '')); ?></span>
                                            </p>


                                            <input type="hidden" class="total-field" name="total"
                                                   value="<?php echo e(round($invoice->total, 2)); ?>">
                                        </div>

                                    </div>

                            </div>


                        </div>
                        <div class="form-actions" style="margin-top: 70px">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" id="save-form" class="btn btn-success"><i
                                                class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    
    <div class="modal fade bs-modal-md in" id="taxModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>

<script>
    $(function () {
        $( "#sortable" ).sortable();
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });

    jQuery('#invoice_date, #due_date').datepicker({
        format: '<?php echo e($global->date_picker_format); ?>',
        autoclose: true,
        todayHighlight: true
    });

    $('#save-form').click(function(){
        calculateTotal();

        var discount = $('.discount-amount').html();
        var total = $('.total-field').val();

        if(parseFloat(discount) > parseFloat(total)){
            $.toast({
                heading: 'Error',
                text: 'Discount cannot be more than total amount.',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
            return false;
        }

        $.easyAjax({
            url:'<?php echo e(route('admin.all-invoices.store')); ?>',
            container:'#storePayments',
            type: "POST",
            redirect: true,
            data:$('#storePayments').serialize()
        })
    });
    $('#add-item').click(function () {
        var i = $(document).find('.item_name').length;
        var item = '<div class="col-xs-12 item-row margin-top-5">'+
                    '<div class="col-md-4">'+
                    '<div class="row">'+
                    '<div class="form-group">'+
                    '<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.item'); ?></label>'+
                    '<div class="input-group">'+
                    '<div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>'+
                    '<input type="text" class="form-control item_name" name="item_name[]">'+
                    '</div>'+
                    '</div>'+

                    '<div class="form-group">'+
                    '<textarea name="item_summary[]" class="form-control" placeholder="<?php echo app('translator')->get('app.description'); ?>" rows="2"></textarea>'+
                    '</div>'+

                    '</div>'+

                    '</div>'+

                    '<div class="col-md-1">'+

                    '<div class="form-group">'+
                    '<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.qty'); ?></label>'+
                    '<input type="number" min="1" class="form-control quantity" value="1" name="quantity[]" >'+
                    '</div>'+


                    '</div>'+

                    '<div class="col-md-2">'+
                    '<div class="row">'+
                    '<div class="form-group">'+
                    '<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.unitPrice'); ?></label>'+
                    '<input type="text"  class="form-control cost_per_item" name="cost_per_item[]" value="0" >'+
                    '</div>'+
                    '</div>'+

                    '</div>'+

                    '<div class="col-md-2">'+

            '<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.tax'); ?></label>'
            +'<select id="multiselect'+i+'" name="taxes['+i+'][]"  multiple="multiple" class="selectpicker form-control type">'
                <?php $__currentLoopData = $taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            +'<option data-rate="<?php echo e($tax->rate_percent); ?>" value="<?php echo e($tax->id); ?>"><?php echo e($tax->tax_name.': '.$tax->rate_percent); ?>%</option>'
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            +'</select>'
            +'</div>'+


                '</div>'+

                '<div class="col-md-2 border-dark  text-center">'+
                    '<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.amount'); ?></label>'+

                    '<p class="form-control-static"><span class="amount-html">0.00</span></p>'+
                '<input type="hidden" class="amount" name="amount[]">'+
                    '</div>'+

                    '<div class="col-md-1 text-right visible-md visible-lg">'+
                    '<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'+
                '</div>'+
                '<div class="col-xs-12 text-center hidden-md hidden-lg">'+
                    '<div class="row">'+
                    '<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'+
                '</div>'+
                '</div>'+

                '</div>';

        $(item).hide().appendTo("#sortable").fadeIn(500);
        $('#multiselect'+i).selectpicker();
    });

    $('#storePayments').on('click','.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function() {
            $(this).remove();
            calculateTotal();
        });
    });

    $('#storePayments').on('keyup','.quantity, .cost_per_item, .item_name, .discount_value', function () {
        var quantity = $(this).closest('.item-row').find('.quantity').val();

        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

        var amount = (quantity*perItemCost);

        $(this).closest('.item-row').find('.amount').val(amount);
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

        calculateTotal();


    });

    $('#storePayments').on('change','.type, #discount_type', function () {
        var quantity = $(this).closest('.item-row').find('.quantity').val();

        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

        var amount = (quantity*perItemCost);

        $(this).closest('.item-row').find('.amount').val(amount);
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

        calculateTotal();
    });

    function calculateTotal()
    {
        var subtotal = 0;
        var discount = 0;
        var tax = '';
        var taxList = new Object();
        var taxTotal = 0;
        $(".quantity").each(function (index, element) {
            var itemTax = [];
            var itemTaxName = [];
            $(this).closest('.item-row').find('select.type option:selected').each(function (index) {
                itemTax[index] = $(this).data('rate');
                itemTaxName[index] = $(this).text();
            });
            var itemTaxId = $(this).closest('.item-row').find('select.type').val();
            var amount = $(this).closest('.item-row').find('.amount').val();
            subtotal = parseFloat(subtotal)+parseFloat(amount);

            if(itemTaxId != ''){
                for(var i = 0; i<=itemTaxName.length; i++)
                {
                    if(typeof (taxList[itemTaxName[i]]) === 'undefined'){
                        taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                    }
                    else{
                        taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                    }
                }
            }
        });

        $.each( taxList, function( key, value ) {
            if(!isNaN(value)){

                tax = tax+'<div class="col-md-offset-8 col-md-2 col-xs-6 text-right p-t-10">'
                    +key
                    +'</div>'
                    +'<p class="form-control-static col-xs-6 col-md-2" >'
                    +'<span class="tax-percent">'+decimalupto2(value)+'</span>'
                    +'</p>';

                taxTotal = taxTotal+value;
            }

        });

        $('.sub-total').html(decimalupto2(subtotal));
        $('.sub-total-field').val(decimalupto2(subtotal));


        var discountType = $('#discount_type').val();
        var discountValue = $('.discount_value').val();

        if(discountValue != ''){
            if(discountType == 'percent'){
                discount = ((parseFloat(subtotal)/100)*parseFloat(discountValue));
            }
            else{
                discount = parseFloat(discountValue);
            }

        }

        $('#invoice-taxes').html(tax);

        var totalAfterDiscount = subtotal-discount;

        totalAfterDiscount = (totalAfterDiscount < 0) ? 0 : totalAfterDiscount;

        var total = decimalupto2(totalAfterDiscount+taxTotal);

        $('.total').html(total);
        $('.total-field').val(total);

    }
    calculateTotal();

    function recurringPayment() {
        var recurring = $('#recurring_payment').val();

        if(recurring == 'yes')
        {
            $('.recurringPayment').show().fadeIn(300);
        } else {
            $('.recurringPayment').hide().fadeOut(300);
        }
    }

    $('#tax-settings').click(function () {
        var url = '<?php echo e(route('admin.taxes.create')); ?>';
        $('#modelHeading').html('Manage Project Category');
        $.ajaxModal('#taxModal', url);
    });

    function decimalupto2(num) {
        var amt =  Math.round(num * 100) / 100;
        return parseFloat(amt.toFixed(2));
    }


    $('.add-product').on('click', function(event) {
        event.preventDefault();
        var id = $(this).data('pk');
        $.easyAjax({
            url:'<?php echo e(route('admin.all-invoices.update-item')); ?>',
            type: "GET",
            data: { id: id },
            success: function(response) {
                $(response.view).hide().appendTo("#sortable").fadeIn(500);
                var noOfRows = $(document).find('#sortable .item-row').length;
                var i = $(document).find('.item_name').length-1;
                var itemRow = $(document).find('#sortable .item-row:nth-child('+noOfRows+') select.type');
                itemRow.attr('id', 'multiselect'+i);
                itemRow.attr('name', 'taxes['+i+'][]');
                $(document).find('#multiselect'+i).selectpicker();
            }
        });
    });


</script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/proposals/convert_proposal.blade.php ENDPATH**/ ?>