<li class="top-notifications">
    <div class="message-center">
        <a href="javascript:;" class="show-all-notifications">
            <div class="user-img">
                <span class="btn btn-circle btn-success"><i class="fa fa-tasks"></i></span>
            </div>
            <div class="mail-contnet">
                <span class="mail-desc m-0"><?php echo e(ucfirst($notification->data['heading'])); ?> - <?php echo app('translator')->get('email.taskComplete.subject'); ?>!</span> <span class="time"><?php if($notification->data['completed_on']): ?><?php echo e(\Carbon\Carbon::parse( $notification->data['completed_on'])->diffForHumans()); ?><?php endif; ?></span>
            </div>
        </a>
    </div>
</li>
<?php /**PATH C:\wamp64\www\wtc_emp\resources\views/notifications/member/task_completed.blade.php ENDPATH**/ ?>