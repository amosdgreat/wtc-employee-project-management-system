<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e($pageTitle); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"><?php echo e($pageTitle); ?></div>

                <div class="vtabs customvtab">
                    <?php echo $__env->make('sections.admin_setting_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="row">
                        <div class="white-box">
                            <?php echo Form::open(['id'=>'editSettings','class'=>'ajax-form','method'=>'POST']); ?>

                            <div class="col-sm-12 m-t-10">

                                <div class="form-group">
                                    <div class="checkbox checkbox-info  col-md-10">
                                        <input id="self_task" name="self_task" value="yes"
                                                <?php if($global->task_self == "yes"): ?> checked
                                                <?php endif; ?>
                                                type="checkbox">
                                        <label for="self_task"><?php echo app('translator')->get('messages.employeeSelfTask'); ?></label>
                                    </div>
                                </div>

                            </div>
                            
                            <div class="col-sm-12">
                                <h4><?php echo app('translator')->get('modules.tasks.reminder'); ?></h4>
                            </div>

                            <div class="col-md-3">

                                <div class="form-group">
                                    <label class="control-label"><?php echo app('translator')->get('modules.tasks.preDeadlineReminder'); ?>  (<?php echo app('translator')->get('app.days'); ?>)</label>
                                    <input type="number" value="<?php echo e($global->before_days); ?>" min="0" class="form-control" name="before_days"> 
                                </div>

                            </div>

                            <div class="col-md-3">

                                <div class="form-group">
                                    <label class="control-label"><?php echo app('translator')->get('modules.tasks.onDeadlineReminder'); ?></label>
                                    <div class="radio-list">
                                        <label class="radio-inline p-0">
                                            <div class="radio radio-info">
                                                <input type="radio" name="on_deadline" <?php if($global->on_deadline == 'yes'): ?> checked  <?php endif; ?> id="on_deadline_yes" value="yes">
                                                <label for="on_deadline_yes"><?php echo app('translator')->get('app.yes'); ?></label>
                                            </div>
                                        </label>
                                        <label class="radio-inline">
                                            <div class="radio radio-info">
                                                <input type="radio" name="on_deadline" <?php if($global->on_deadline == 'no'): ?> checked  <?php endif; ?> id="on_deadline_no" value="no">
                                                <label for="on_deadline_no"><?php echo app('translator')->get('app.no'); ?></label>
                                            </div>
                                        </label>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <div class="form-group">
                                    <label class="control-label"><?php echo app('translator')->get('modules.tasks.postDeadlineReminder'); ?>  (<?php echo app('translator')->get('app.days'); ?>)</label>
                                    <input type="number" value="<?php echo e($global->after_days); ?>" min="0" class="form-control" name="after_days"> 
                                </div>

                            </div>

                            <div class="col-md-3">

                                <div class="form-group">
                                    <label class="control-label"><?php echo app('translator')->get('modules.tasks.defaultTaskStatus'); ?></label>
                                    <select name="default_task_status" class="form-control" id="default_task_status">
                                        <?php $__currentLoopData = $taskboardColumns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option
                                            <?php if($item->id == $global->default_task_status): ?>
                                                selected
                                            <?php endif; ?> 
                                            value="<?php echo e($item->id); ?>"><?php echo e($item->column_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>

                            </div>

                            <div class="col-sm-12">
                                <button class="btn btn-success" id="save-form" type="button"><?php echo app('translator')->get('app.save'); ?></button>
                            </div>

                            <?php echo Form::close(); ?>

                        </div>
                    </div>
                    <!-- /.row -->

                            
                </div>

            </div>
        </div>


    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>

    <script>
        // change task Setting For Setting
        $('#save-form').click(function () {

            $.easyAjax({
                url: '<?php echo e(route('admin.task-settings.store')); ?>',
                container: '#editSettings',
                type: "POST",
                data: $('#editSettings').serialize()               
            })
            
        });

    </script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/task-settings/edit.blade.php ENDPATH**/ ?>