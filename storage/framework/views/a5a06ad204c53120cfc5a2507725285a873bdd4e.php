<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title"><?php echo app('translator')->get('app.addNew'); ?> <?php echo app('translator')->get('modules.lead.leadSource'); ?></h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        <?php echo Form::open(['id'=>'addLeadSource','class'=>'ajax-form','method'=>'POST']); ?>

        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label class="required"><?php echo app('translator')->get('modules.lead.leadSource'); ?></label>
                        <input type="text" name="type" id="type" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-group" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>
        </div>
        <?php echo Form::close(); ?>

    </div>
</div>

<script>

    // Store lead source
    $('#save-group').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.lead-source-settings.store')); ?>',
            container: '#addLeadSource',
            type: "POST",
            data: $('#addLeadSource').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    var options = [];
                    var rData = [];
                    rData = response.optionData;
                    $('#source_id').html(rData);
                    $("#source_id").select2();
                    $('#projectCategoryModal').modal('hide');
                }
            }
        })
    });
</script>
<?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/lead-settings/source/create-modal.blade.php ENDPATH**/ ?>