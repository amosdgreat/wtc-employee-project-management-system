<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" ><i class="icon-clock"></i> <?php echo app('translator')->get('app.menu.attendance'); ?> <?php echo app('translator')->get('app.details'); ?> </h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="card punch-status">
                <div class="white-box">
                    <h4><?php echo app('translator')->get('app.menu.attendance'); ?> <small class="text-muted"><?php echo e($startTime->format($global->date_format)); ?></small></h4>
                    <div class="punch-det">
                        <h6><?php echo app('translator')->get('modules.attendance.clock_in'); ?></h6>
                        <p><?php echo e($startTime->format($global->time_format)); ?></p>
                    </div>
                    <div class="punch-info">
                        <div class="punch-hours">
                            <span><?php echo e($totalTime); ?> hrs</span>
                        </div>
                    </div>
                    <div class="punch-det">
                        <h6><?php echo app('translator')->get('modules.attendance.clock_out'); ?></h6>
                        <p><?php echo e($endTime->format($global->time_format)); ?> 
                        <?php if(isset($notClockedOut)): ?>
                            (<?php echo app('translator')->get('modules.attendance.notClockOut'); ?>)
                        <?php endif; ?>
                        </p>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card recent-activity">
                <div class="white-box">
                    <h5 class="card-title"><?php echo app('translator')->get('modules.employees.activity'); ?></h5>

                        <?php $__currentLoopData = $attendanceActivity->reverse(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <div class="row res-activity-box" id="timelogBox<?php echo e($item->aId); ?>">
                            <ul class="res-activity-list col-md-9">
                                <li>
                                    <p class="mb-0"><?php echo app('translator')->get('modules.attendance.clock_in'); ?></p>
                                    <p class="res-activity-time">
                                        <i class="fa fa-clock-o"></i>
                                        <?php echo e($item->clock_in_time->timezone($global->timezone)->format($global->time_format)); ?>.
                                    </p>
                                </li>
                                <li>
                                    <p class="mb-0"><?php echo app('translator')->get('modules.attendance.clock_out'); ?></p>
                                    <p class="res-activity-time">
                                        <i class="fa fa-clock-o"></i>
                                        <?php if(!is_null($item->clock_out_time)): ?>
                                            <?php echo e($item->clock_out_time->timezone($global->timezone)->format($global->time_format)); ?>.
                                        <?php else: ?>
                                            <?php echo app('translator')->get('modules.attendance.notClockOut'); ?>
                                        <?php endif; ?>
                                    </p>
                                </li>
                            </ul>

                             <div class="col-md-3">
                                 <a href="javascript:;" onclick="editAttendance(<?php echo e($item->aId); ?>)" style="display: inline-block;" id="attendance-edit" data-attendance-id="<?php echo e($item->aId); ?>" ><label class="label label-info"><i class="fa fa-pencil"></i> </label></a>
                                 <a href="javascript:;" onclick="deleteAttendance(<?php echo e($item->aId); ?>)" style="display: inline-block;" id="attendance-edit" data-attendance-id="<?php echo e($item->aId); ?>" ><label class="label label-danger"><i class="fa fa-times"></i></label></a>
                             </div>
                         </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </div>
    </div>

</div>
<script>
     function deleteAttendance(id){
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted user!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                var url = "<?php echo e(route('admin.attendances.destroy',':id')); ?>";
                url = url.replace(':id', id);

                var token = "<?php echo e(csrf_token()); ?>";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();

                            $('#timelogBox'+id).remove();
//                                    swal("Deleted!", response.message, "success");
                            showTable();
                            $('#projectTimerModal').modal('hide');
                        }
                    }
                });
            }
        });
    }

</script><?php /**PATH C:\wamp64\www\wtc_emp\resources\views/admin/attendance/attendance_info.blade.php ENDPATH**/ ?>